# SPDX-License-Identifier: GPLv3-or-later
#
# SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>
# SPDX-FileCopyrightText: Patrick L.S. Connor <connorpa@mail.desy.de>

# Libraries
add_library(natsort OBJECT strnatcmp.c)
target_include_directories(natsort PUBLIC "${CMAKE_CURRENT_LIST_DIR}")
install(FILES strnatcmp.h DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/Darwin)
