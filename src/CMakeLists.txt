# SPDX-License-Identifier: GPLv3-or-later
#
# SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")

# Generate the version header with the current git hash
# This is a bit complicated because we need to run git at build time, and this
# file is executed at configure time. So we need to add a custom target to the
# build system. This is done below using the helper script CreateVersionHeader
# found in the cmake/ directory.
set(version_header "${CMAKE_CURRENT_BINARY_DIR}/version.h")
add_custom_target(CreateVersionHeader
                  COMMENT "Generating version.h"
                  COMMAND "${CMAKE_COMMAND}"
                          -D GIT_EXECUTABLE="${GIT_EXECUTABLE}"
                          -D OUTPUT_PATH="${version_header}"
                          -P "${CMAKE_SOURCE_DIR}/cmake/CreateVersionHeader.cmake"
                  WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
                  BYPRODUCTS "${version_header}")

# Darwin library
add_library(Darwin SHARED
            FileUtils.cc
            Flow.cc
            FriendUtils.cc
            Looper.cc
            MetaInfo.cc
            Options.cc
            UserInfo.cc
            Variation.cc
            "${version_header}"
            $<TARGET_OBJECTS:natsort>)
root_generate_dictionary(Dict_rdict classes.h
                         MODULE Darwin
                         LINKDEF "${CMAKE_SOURCE_DIR}/interface/LinkDef.h")

target_include_directories(Darwin PRIVATE "${CMAKE_CURRENT_BINARY_DIR}")
target_link_libraries(Darwin PRIVATE natsort)
target_link_libraries(Darwin PUBLIC
        Boost::boost Boost::program_options Boost::system
        LibGit2::LibGit2
        ROOT::Hist ROOT::MathCore ROOT::Tree)

install(TARGETS Darwin
        EXPORT DarwinTargets
        COMPONENT ${CMAKE_PROJECT_NAME}
        RUNTIME  DESTINATION "${CMAKE_INSTALL_LIBDIR}"
        INCLUDES DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/Darwin")
