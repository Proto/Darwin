#include "Variation.h"

// natsort
#include <strnatcmp.h>

using namespace std;

namespace Darwin::Physics {

std::strong_ordering operator<=> (const Variation& lhs, const Variation& rhs)
{
    int c = strnatcmp(lhs.Name().data(), rhs.Name().data()); // natural sortering
    auto tlhs = make_tuple(lhs.Group(), lhs.Index(), 0, lhs.Bit());
    auto trhs = make_tuple(rhs.Group(), rhs.Index(), c, rhs.Bit());
    return tlhs <=> trhs;
}

} // end of namespace Darwin::Physics

std::ostream& operator<< (std::ostream& s, const Darwin::Physics::Variation& v)
{
    using namespace Darwin::Physics;
    // if (v == nominal) return s << v.Name(); // TODO
    return s << v.Group() << '[' << v.Index() << "]=" << v.Name() << '(' << v.Bit() << ')';
}
