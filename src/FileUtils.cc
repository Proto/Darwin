#include "colours.h"
#include "FileUtils.h"
#include "FriendUtils.h"

#include <TFile.h>
#include <TKey.h>
#include <TTree.h>
#include <TSystem.h>

#include <strnatcmp.h>

#include <cstdlib>
#include <iostream>

using namespace std;
namespace fs = filesystem;
namespace DE = Darwin::Exceptions;

namespace Darwin::Tools {

std::unique_ptr<TFile> GetOutput (std::filesystem::path output,
                                  const std::string& name)
{
    const auto fname = output;
    output += "?reproducible="; output += name;
    std::unique_ptr<TFile> f(TFile::Open(output.c_str(), "RECREATE"));
    if (!f || !f->IsOpen() || f->IsZombie() || !fs::exists(fname))
        BOOST_THROW_EXCEPTION( fs::filesystem_error("Failed to open output file",
                                output, make_error_code(std::errc::io_error)) );
    return f;
}

namespace {
    
////////////////////////////////////////////////////////////////////////////////
/// \brief ROOT error handler.
///
/// (Debug) information and warnings are simply displayed in colour while
/// (fatal) errors are converted to proper exceptions.
///
/// \note [Original handler](https://github.com/root-project/root/blob/c31714f652bee29503a2395281542037171154e9/core/base/src/TErrorDefaultHandler.cxx#L102)
void root_error_handler (int level, bool request_abort, const char * location, const char * msg)
{
    if (level < gErrorIgnoreLevel)
        return;

    using namespace std;
    string s = location + ": "s + msg;
    if (request_abort) {
        cerr << red << s << "\nROOT has requested abortion.\n" << def;
        if (gSystem) {
            gSystem->StackTrace();
            gSystem->Abort();
        }
        else
            abort();
    }
    else if (level <= kWarning)
        cerr << orange << s << '\n' << def;
    else {
        string what = Form("%s%s%s", bold, msg, normal);
        boost::throw_exception( invalid_argument(what),
                boost::source_location(__FILE__, __LINE__, location));
        /// \note The file and line numbers do not correspond to the original location.
    }
}

} // end of anonymous namespace

void StandardInit ()
{
    gROOT->SetBatch();
    TH1::SetDefaultSumw2();
    TH1::AddDirectory(kFALSE);
    SetErrorHandler(root_error_handler);
}

std::vector<std::filesystem::path> GetROOTfiles
        (std::vector<std::filesystem::path> inputs) //!< ROOT files or directories
{
    vector<fs::path> root_inputs;
    root_inputs.reserve(inputs.size());

    for (auto input: inputs) {
        input = fs::absolute(input);
        if (fs::is_directory(input)) {
            for (const auto &entry : fs::recursive_directory_iterator(
                    input, fs::directory_options::follow_directory_symlink)) {
                if (fs::is_regular_file(entry) && entry.path().extension() == ".root") {
                    root_inputs.push_back(entry);
                }
            }
        }
        else if (input.extension() == ".root")
            root_inputs.push_back(input);
    }

    sort(root_inputs.begin(), root_inputs.end(),
            [](const fs::path& a, const fs::path& b) {
                return strnatcmp(a.c_str(), b.c_str()) < 0;
            });
    return root_inputs;
}

std::string GetFirstTreeLocation
                (const std::filesystem::path& input) //!< one input ROOT file
{
    unique_ptr<TFile> fIn(TFile::Open(input.c_str(), "READ"));
    if (!fIn || !fIn->IsOpen() || fIn->IsZombie() || !fs::exists(input))
        BOOST_THROW_EXCEPTION( fs::filesystem_error("Failed to open input file",
                                input, make_error_code(std::errc::io_error)) );

    function<string(TDirectory *)> loop;
    loop = [&loop](TDirectory * d) -> string {

        // look for a `TTree` in the current `TDirectory`
        for (const auto&& obj: *(d->GetListOfKeys())) {
            auto const key = dynamic_cast<TKey*>(obj);
            if (dynamic_cast<TTree *>(key->ReadObj()) == nullptr) continue;
            return obj->GetName();
        }

        // if not found, check all sub`TDirectory`s
        for (const auto&& obj: *(d->GetListOfKeys())) {
            auto const key = dynamic_cast<TKey*>(obj);
            auto dd = dynamic_cast<TDirectory *>(key->ReadObj());
            if (dd == nullptr) continue;
            if (auto location = loop(dd); !location.empty())
                return Form("%s/%s", dd->GetName(), location.c_str());
        }

        // if nothing was found at this stage, return empty string
        return string();
    };
    string location = loop(fIn.get());
    if (location.empty())
        BOOST_THROW_EXCEPTION( DE::BadInput("No `TTree` could be found in the input file.", fIn) );
    return location;
}

std::shared_ptr<TFile> GetOutputFile (const std::filesystem::path& output,
                                      const std::source_location location)
{
    string title = fs::path(location.file_name()).stem();
    auto output_rep = output.string() + "?reproducible="s + title;
    auto fOut = make_shared<TFile>(output_rep.c_str(), "RECREATE");
    if (!fOut->IsOpen() || fOut->IsZombie() || !fs::exists(output)) {
        fOut.reset();
        BOOST_THROW_EXCEPTION( fs::filesystem_error("Failed to open output file",
                                output, make_error_code(errc::io_error)) );
    }
    fOut->SetTitle(title.c_str());
    return fOut;
}

}
