/// SPDX-License-Idendifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

#include "FriendUtils.h"

#include "colours.h"
#include "exceptions.h"

#include <TChainElement.h>

#include <iostream>

using namespace std;

using namespace Darwin::Tools;

void ChainSlice::Print (Option_t *opt) const
{
    TNamed::Print(opt);

    if (fTree) {
        Printf("Printing current tree, corresponding to slice [%lld:%lld)", fBegin, fEnd);
        this->GetTree()->Print(opt);
    } else
        Printf("No tree to print, slice [%lld:%lld)", fBegin, fEnd);
}

/**
 * \brief Destructor.
 */
ChainSlice::~ChainSlice()
{}

/**
 * \brief Constructs an SlicedFriendElement without attaching it to the parent.
 *
 * See the corresponding \c TFriendElement constructor.
 */
SlicedFriendElement::SlicedFriendElement (
        TTree * parent, ChainSlice * friendChain, const char * alias)
    : TFriendElement(parent, friendChain, alias)
    , fBegin(friendChain->GetBegin())
    , fEnd(friendChain->GetEnd())
    , fTrees(new TList)
    , fChain(friendChain)
    , fOwnsChain(false)
    , fNotify(this)
{
    for (const TObject * el : *friendChain->GetListOfFiles()) {
        // These are actually TChainElements, but we don't need to know
        auto named = dynamic_cast<const TNamed *>(el);
        if (!named)
            // If this throws, ROOT changed how chains work
            BOOST_THROW_EXCEPTION(
                std::logic_error("unexpected chain structure"));

        auto name = TString(el->GetTitle()) + "?#" + el->GetName();
        fTrees->Add(new TObjString(name));
    }
}

/**
 * \brief Destructor.
 */
SlicedFriendElement::~SlicedFriendElement ()
{
    if (fOwnsChain)
        delete fChain;
    delete fTrees;
}

/**
 * \brief Adds a \c ChainSlice as a friend to a \c TTree.
 *
 * The chain is added to the list of friends of the tree. Entry 0 in the tree
 * is matched to entry 0 in the chain, taking the offset into account. The
 * chain offset must be set before using this function or it will not be stored
 * correctly.
 */
SlicedFriendElement * SlicedFriendElement::AddTo (TTree * tree,
                                                  ChainSlice * chain,
                                                  const char * alias)
{
    // Make sure the tree has a list of friends. This requires some gymnastics
    // when it's not there yet.
    auto friends = tree->GetListOfFriends();
    if (!friends) {
        // Force the TTree to create its list of friends by adding one.
        TTree t;
        tree->AddFriend(&t);
        tree->RemoveFriend(&t);
        friends = tree->GetListOfFriends();
        if (!friends)
            // Normally the above will always work, but ROOT might change it at
            // some point
            BOOST_THROW_EXCEPTION(
                std::logic_error("could not create TTree list of friends"));
    }

    auto element = new SlicedFriendElement(tree, chain, alias);
    friends->Add(element);
    /// \todo ROOT does a few extra checks when adding a friend.
    ///       Easier to do if upstreaming.

    return element;
}

/**
 * \brief See \c TFriendElement::GetTree.
 *
 * This returns a \ref ChainSlice.
 */
TTree * SlicedFriendElement::GetTree ()
{
    if (!fChain) {
        fChain = new ChainSlice(GetName(), GetBegin(), GetEnd(), GetTitle());
        fChain->RegisterExternalFriend(this);
        fOwnsChain = true;
        if (fTrees)
            fChain->AddFileInfoList(fTrees);
    } else {
        // See the HACK section below.
        ResetUpdated();
    }

    // HACK: Ask the parent tree to notify us when a new tree is loaded.
    //
    // This is necessary when our parent is in a TChain. It allows us to work
    // around a ROOT bug and force trigger internal TChain code to update
    // branch addresses.
    //
    // If the parent is a simple TTree and we are the only friend, fNotify
    // fires in two cases:
    //
    // * Upon loading of the parent TTree. This only ever happens once.
    // * When *our* chain switches to another tree. This is what we need to
    //   catch.
    //
    // The sequence of events for the second case is as follows:
    //
    // 1. Our chain loads a new tree and calls MarkUpdated().
    // 2. The parent tree checks IsUpdated() then ResetUpdated().
    // 3. If IsUpdated() returned true, call our Notify().
    // 4. Our Notify() calls MarkUpdated() again.
    // 5. The parent's chain checks IsUpdated() then calls ResetUpdated(). If
    //    we hadn't marked ourselves as updated, IsUpdated() would return false
    //    here and nothing else would happen.
    // 6. The parent's chain, seeing IsUpdated() == true, updates its branch
    //    addresses. This is the logic we need to trigger.
    //
    // If our parent tree isn't in a TChain, IsUpdated() will never get reset
    // externally. We do it above to avoid Notify being called at every event
    // (this function gets called for every event).
    //
    // We do not do this when the parent is a TChain because direct friends of
    // a TChain update branches in step 2 above.
    if (!fNotify.IsLinked() && !dynamic_cast<TChain *>(GetParentTree())) {
        fNotify.PrependLink(*GetParentTree());
    }

    return fChain;
}

/**
 * \brief Always returns \c nullptr, as \ref ChainSlice cover multiple files.
 */
TFile * SlicedFriendElement::GetFile ()
{
    return nullptr;
}

/**
 * \brief Gets notification about modified TTrees.
 */
bool SlicedFriendElement::Notify ()
{
    // If the parent tree is part of a TChain, the chain may look at us next if
    // configured properly.  Even then it doesn't always do it, so we may stay
    // in updated state for too long, triggering extraneous branch updates.
    // Fixing the behaviour in ROOT would allow removing this hack.
    //
    // See the explanation in GetTree() for why we do this.
    MarkUpdated();

    return true;
}

/**
 * \brief Prints information about the trees in this friend element.
 */
void SlicedFriendElement::Print (Option_t *opt) const
{
    TNamed::Print(opt);
    if (fTrees) {
        Printf("Contains %d trees, slice [%lld:%lld):", fTrees->GetSize(), fBegin, fEnd);
        for (const TObject * tree : *fTrees)
            Printf("- %s", tree->GetName());
    } else
        Printf("Contains no trees, slice [%lld:%lld)", fBegin, fEnd);
}
