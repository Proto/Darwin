#include <cstdlib>

#include <iostream>
#include <iomanip>
#include <regex>
#include <system_error>
#include <filesystem>
#include <string>
#include <numeric>

#include <boost/algorithm/string.hpp>

#include <boost/version.hpp>
#include <boost/program_options.hpp>
#include <boost/exception/all.hpp>

#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "colours.h"
#include "Options.h"
#include "version.h"


using namespace std;
namespace fs = filesystem;
namespace po = boost::program_options; // used to read the options from command line
namespace pt = boost::property_tree;
namespace al = boost::algorithm;

using namespace Darwin::Tools;

string Options::full_cmd;
fs::path Options::prefix;

void Options::check_input (const std::filesystem::path& input)
{
    if (!fs::exists(input))
        BOOST_THROW_EXCEPTION(fs::filesystem_error("Bad input", input,
                    make_error_code(errc::no_such_file_or_directory)));

    full_cmd += ' ' + fs::canonical(input).string();

    if ((fs::status(input).permissions() & fs::perms::owner_read) == fs::perms::none)
        BOOST_THROW_EXCEPTION(fs::filesystem_error("Input exists but cannot be read",
                    input, make_error_code(errc::permission_denied)));
}

void Options::check_output (const std::filesystem::path& output)
{
    fs::path outputHist = output;
    if (!prefix.empty() && !fs::is_directory(output)) outputHist.remove_filename();
    full_cmd += ' ' + outputHist.string();

    if (!fs::exists(output)) return;

    if ((fs::status(output).permissions() & fs::perms::owner_write)
                                         == fs::perms::none) {
        full_cmd.clear();
        BOOST_THROW_EXCEPTION(fs::filesystem_error(
                    "Output already exists and cannot be overwritten",
                    output, make_error_code(errc::permission_denied)));
    }

    if (fs::is_directory(output) && fs::equivalent(output,".")) {
        full_cmd.clear();
        BOOST_THROW_EXCEPTION(fs::filesystem_error("Not a valid output",
                    output, make_error_code(errc::invalid_argument)));
    }
    else if (output != "/dev/null")
        cerr << orange << "Warning: you are overwriting " << output << def << '\n';
}

string Options::parse_env_var (string s)
{
    static const regex env_re{R"--(\$\{([^}]+)\})--"};
    smatch match;
    while (regex_search(s, match, env_re)) {
        auto const from = match[0];
        auto const name = match[1].str();
        auto const value = getenv(name.c_str());
        if (!value)
            BOOST_THROW_EXCEPTION(runtime_error(
                        "Environment variable ${" + name + "} does not exist."));
        s.replace(from.first, from.second, value);
    }
    return s;
} 

////////////////////////////////////////////////////////////////////////////////
/// Function used by Boost::PO to disable standard error
void set_mute (bool flag)
{
    if (flag) cout.setstate(ios_base::failbit); 
}

Options::Options (const std::string& tuto,
                  int pars,
                  const char* commit,
                  const char* example)
    : hidden{"Hidden"}, helper{"Helper"}, common{"Options"}, custom{"Positional options"},
      example(example ? example : DARWIN "/test/example.info"),
      tutorial(tuto), stage(Stage::Input), params(pars),
      m_commit(commit ? commit : DARWIN_VERSION),
      steer(0), j(1), k(0)
{
    hidden.add_options()
        ("prefix,p", po::value<fs::path>(&prefix), "Prefix command, `parallel` or `submit` (for history only)");

    // first the helper
    helper.add_options()
        ("help,h", "Help screen (what you are seeing right now)")
        ("tutorial,t", "Brief description of the command's purpose")
        ("git,g", "Commit hash of this executable at compilation time")
        ("input_ext,i", "Expected extension for input(s) (one line for each input)")
        ("output_ext,o", "Expected extension for output(s) (one line for each output)");
    if (params & config) helper.add_options()
        ("example,e", "Print config example");

    // then the running options
    common.add_options()
        ("verbose,v", po::bool_switch()->default_value(false),
         "Enable debug mode (typically in slow operations such as the event loop)")
        ("mute,m", po::bool_switch()->default_value(false)->notifier(set_mute),
         "Disable standard output stream");
    if (params & config) common.add_options()
        ("config,c", po::value<fs::path>(&config_file),
         "Configuration file in INFO, JSON, or XML format");
    if (params & split) common.add_options()
        ("nSplit,j", po::value<int>(&j)->default_value(j), "Number of slices")
        ("nNow,k"  , po::value<int>(&k)->default_value(k), "Index of current slice");
    if (params & (fill | Friend)) common.add_options()
        ("fill,f", po::bool_switch()->default_value(false), "Enable `TTree::Fill()`");
    if (params & Friend) common.add_options()
        ("Friend,F", po::bool_switch()->default_value(false),
         "Use `TTree` friends instead of cloning the whole input `TTree`");
    if (params & syst) common.add_options()
        ("syst,s", po::bool_switch()->default_value(false),
         "Enable systematic variations");

    // positional arguments should be defined by hand by the user
    // with the various public members
}

////////////////////////////////////////////////////////////////////////////////
/// Wraps a given paragraph to fit within the same number of characters as the
/// standard helper message from Boost Program Options.
string wrap_paragraph (const string& text)
{
    if (text.size() == 0) return text;

    vector<string> paragraphs;
    al::split(paragraphs, text, al::is_any_of("\n"));

    string wrapped;
    for (auto const& paragraph: paragraphs) {
        vector<string> words;
        al::split(words, paragraph, al::is_any_of(" \t"), al::token_compress_on);

        string line;
        for (auto const& word: words) {
            size_t s = line.size() + word.size();
            static const size_t sMax = po::options_description::m_default_line_length;

            if (s > sMax) {
                line.pop_back();
                wrapped += line;
                wrapped += '\n';
                line.clear();
            }

            line += word;
            line += ' ';
        }
        line.pop_back();
        wrapped += line + '\n';
    }

    return wrapped;
}

void Options::parse_helper (int argc, const char * const argv[])
{
    po::command_line_parser parser{argc, argv};
    parser.options(helper) // parse only the helper, but no other option at this stage
          .allow_unregistered(); // ignore unregistered options

    // defines actions
    po::variables_map vm;
    po::store(parser.run(), vm);
    po::notify(vm); // necessary for config to be given the value from the cmd line

    if (vm.count("help") || argc == 1) {
        fs::path executable = argv[0];
        cout << bold << executable.filename().string() << synopsis << def
             << "\nwhere";
        for (const auto& option: custom.options())
            cout << '\t' << option->long_name()
                 << " = " << option->description() << '\n';
    }

    if (vm.count("help")) {
        po::options_description cmd_line;
        cmd_line.add(helper)
                .add(common); // only used to display the helper message,
                              // but not to parse
        cout << cmd_line << endl;
    }

    if (vm.count("tutorial"))
        cout << wrap_paragraph(tutorial) << '\n'
             << wrap_paragraph("General remarks: "
             "unless stated otherwise, all options except the input and output files "
             "may be given either from the command line or from such a config. Unused "
             "options in the config are simply ignored. The `flags` should be set at "
             "the creation of the n-tuples; the `corrections` should be added step "
             "by step (typically a couple of corrections per executable at most). How "
             "the config is parsed may change from executable to executable. This "
             "example uses the Boost INFO format, but JSON and XML formats are also "
             "possible. It is possible to extract such a config from an existing ROOT "
             "file by using `getMetaInfo`. Arguments from command line overwrite "
             "arguments from the config.") << endl;

    if (vm.count("git"))
        cout << commit() << endl;

    if (vm.count("example")) {
        if (!fs::exists(example))
            BOOST_THROW_EXCEPTION(
                    fs::filesystem_error("The example could not be found",
                        example, make_error_code(errc::no_such_file_or_directory)));
        pt::read_info(example, pt_conf);

        pt::ptree reduced_config;
        for (auto const& key: configpaths) {
            auto value = pt_conf.get<string>(key);
            reduced_config.add<string>(key, value);
        }

        stringstream ss;
        write_info(ss, reduced_config);
        string str = ss.str();
        al::erase_all(str, "\"\"");
        cout << str << flush;
    }

    auto space_separate = [](const string& a, const string& b) {
        return a + ' ' + b;
    };

    if (vm.count("input_ext"))
        for (const vector<string>& ext: inputExt)
            if (ext.size() > 0)
                cout << accumulate(next(begin(ext)), end(ext),
                                   ext.front(), space_separate) << endl;

    if (vm.count("output_ext"))
        for (const vector<string>& ext: outputExt)
            if (ext.size() > 0)
                cout << accumulate(next(begin(ext)), end(ext),
                                   ext.front(), space_separate) << endl;

    if (!vm.empty() || argc == 1)
        exit(EXIT_SUCCESS);
}

void Options::parse_common (int argc, const char * const argv[])
{
    po::options_description cmd_line;
    cmd_line.add(common)
            .add(hidden);

    po::command_line_parser parser{argc, argv};
    parser.options(cmd_line)
          .allow_unregistered();

    po::variables_map vm;
    po::store(parser.run(), vm);
    po::notify(vm);

    if (vm.count("nSplit")) {
        auto j = vm["nSplit"].as<int>();
        if (j <= 0)
            BOOST_THROW_EXCEPTION(
                    invalid_argument("The number of slices must be larger than 0") );

        if (vm.count("nNow")) {
            auto k = vm["nNow"].as<int>();
            if (k >= j)
                BOOST_THROW_EXCEPTION(
                        invalid_argument("The slice index must be smaller than the "
                                         "number of slices.") );
        }
    }

    if (vm.count("config")) {
        const string ext = config_file.extension();
        if      (ext == ".json") pt::read_json(config_file.c_str(), pt_conf);
        else if (ext == ".info") pt::read_info(config_file.c_str(), pt_conf);
        else if (ext == ".xml" ) {
            pt::ptree userinfo;
            pt::read_xml(config_file.c_str(), userinfo);
            pt_conf = userinfo.get_child("userinfo");
        }
        else BOOST_THROW_EXCEPTION(
                fs::filesystem_error("Extension should be .json, .xml, or .info",
                                config_file, make_error_code(errc::invalid_argument)));
    }

    if (params & Friend) params |= fill;

    if ((params & fill   ) && vm["fill"   ].as<bool>()) steer |= fill   ;
    if ((params & Friend ) && vm["Friend" ].as<bool>()) steer |= Friend | fill;
    if ((params & syst   ) && vm["syst"   ].as<bool>()) steer |= syst   ;
    if (                      vm["verbose"].as<bool>()) steer |= verbose;
}

void Options::parse_custom (int argc, const char * const argv[])
{
    po::options_description cmd_line;
    cmd_line.add(common) // only here to avoid using `allow_unregistered()`
            .add(hidden) // idem
            .add(custom);

    po::command_line_parser parser{argc, argv};
    parser.options(cmd_line)
          .positional(pos_hide);

    po::variables_map vm;
    po::parsed_options parsed = parser.run();
    po::store(parsed, vm);

    // if no config file is given, then options are all required
    if (!fs::exists(config_file))
    for (auto& name: names) {
        if (vm.count(name)) continue;
        throw po::required_option(name);
    }

    if (!prefix.empty())
        full_cmd = prefix.string() + ' ';
    full_cmd += argv[0];

    po::notify(vm); // necessary for config to be given the value from the cmd line
                    // note: `full_cmd` is modified here

    // first fetch values from config (some may be overwritten later)
    for (auto& configpath: configpaths) {
        auto arg = pt_conf.get_optional<string>(configpath);
        if (!arg) continue;
        al::erase_all(*arg, "\n");
        al::erase_all(*arg, " ");
        al::erase_all(*arg, "\"");
        if (arg->empty()) continue;
        full_cmd += ' ' + *arg;
    }

    if (allow_unregistered()) { // garbage collector

        string configpath = configpaths.back();
        if (!pt_conf.count(configpath))
            pt_conf.add<string>(configpath, "");
        auto& subtree = pt_conf.get_child(configpath);

        for (auto& item: subtree) {
            string key = item.first,
                   value = item.second.get_value<string>();
            if (key.empty() || key == "item") 
                full_cmd += ' ' + value;
            else if (value.empty())
                full_cmd += ' ' + key;
        }

        vector<string> items = po::collect_unrecognized(parsed.options,
                                                    po::include_positional);
        if (items.size() > *registered) {

            items.erase(items.begin(), items.begin() + *registered);

            for (auto& item: items) {
                if (subtree.count(item)) continue;
                subtree.add<string>(item, "");
                full_cmd += ' ' + item;
            }
        }
    } // end of garbage collector

    full_cmd = parse_env_var(full_cmd);
}

void Options::parse_config (pt::ptree& tree, string key)
{
    // loop over the config and parse environment variables
    for (auto& it: tree) {
        string subkey = it.first;
        subkey = parse_env_var(subkey);
        pt::ptree& child = it.second;
        auto value = tree.get_optional<string>(subkey);
        string newkey = key + (key.empty() ? "" : ".") + subkey;
        if (value) {
            *value = parse_env_var(*value);
            tree.put<string>(subkey, *value);
        }
        parse_config(child, newkey);
    }
}

const boost::property_tree::ptree& Options::operator() (int argc,
                                                        const char * const argv[])
{
    pt_conf.clear();
    full_cmd.clear();
    prefix.clear();

    try {
        parse_helper(argc, argv);
        parse_common(argc, argv);
        parse_custom(argc, argv);
        parse_config(pt_conf);

        if (steer & split && j > 1) full_cmd += " -j " + to_string(j);
        if (steer & syst          ) full_cmd += " -s";
        if      (steer & Friend   ) full_cmd += " -F";
        else if (steer & fill     ) full_cmd += " -f";

        pt_conf.put<string>("history", full_cmd);
    }
    catch (const po::error& e) {
        BOOST_THROW_EXCEPTION(po::error(e.what()));
    }

    return pt_conf;
}

Options& Options::set (const char * name,
        const boost::program_options::value_semantic * s, const char * desc)
{
    if (allow_unregistered())
        BOOST_THROW_EXCEPTION(invalid_argument(
                    "Once `Options::args()` has been called, "
                    "it is no longer possible to add any further options."));
    synopsis += ' '; synopsis += name;
    pos_hide.add(name, 1);
    custom.add_options()(name, s, desc);
    return *this;
}

Options& Options::input (const char * name, std::filesystem::path * file,
                        const char * desc, const std::vector<std::string>& ext)
{
    if (stage > Stage::Input)
        BOOST_THROW_EXCEPTION(runtime_error(
                    "Not possible to add another input at this stage"));

    inputExt.push_back(ext);
    const po::value_semantic * s =
        po::value<fs::path>(file)->notifier(check_input)->required();
    return set(name, s, desc);
}

std::string Options::exec (const std::string& cmd)
{
    string result;
    char buffer[128];
    cerr << red;
    FILE * pipe = popen(cmd.c_str(), "r");
    while (!feof(pipe))
        if (fgets(buffer, 128, pipe) != NULL)
            result += buffer;
    pclose(pipe);
    cerr << def;
    return result;
}

Options& Options::inputs (const char * name,
        std::vector<std::filesystem::path> * files, const char * desc,
        const std::vector<std::string>& ext)
{
    if (stage > Stage::Input)
        BOOST_THROW_EXCEPTION(runtime_error(
                    "Not possible to add another input at this stage"));

    auto store = [files,this](const string& pathRegex) {

        const string& ls = exec("ls -1d " + pathRegex);

        vector<fs::path> paths;
        al::split(paths, ls, al::is_any_of("\n"), al::token_compress_on);
        for (fs::path const& p: paths) {
            if (!fs::exists(p)) continue;
            files->push_back(p);
        }

        if (files->empty()) {
            fs::path p = pathRegex;
            full_cmd.clear();
            BOOST_THROW_EXCEPTION(
                    fs::filesystem_error("No input file could be found", p,
                                    make_error_code(errc::no_such_file_or_directory)));
        }

        auto inputs = accumulate(files->begin(), files->end(), string(),
               [](string inputs, fs::path input) {
                            return inputs + fs::canonical(input).string() + ' '; });
        inputs.resize(inputs.size()-1);
        if (files->size() > 1)
            inputs = '"' + inputs + '"';
        full_cmd += ' ' + inputs;
    };

    inputExt.push_back(ext);
    const po::value_semantic * s =
        po::value<string>()->notifier(store)->required();
    string full_desc = desc;
    full_desc += " (use a regular expression, surrounded by quotation marks)";
    return set(name, s, full_desc.c_str());
}

Options& Options::output (const char * name, std::filesystem::path * file,
                        const char * desc, const std::vector<std::string>& ext)
{
    if (stage > Stage::Output) {
        full_cmd.clear();
        BOOST_THROW_EXCEPTION(runtime_error(
                    "Not possible to add another output at this stage"));
    }
    stage = Stage::Output;

    outputExt.push_back(ext);
    const po::value_semantic * s =
        po::value<fs::path>(file)->notifier(check_output)->required();

    return set(name, s, desc);
}

Options& Options::args (const char * name, const char * configpath, const char * desc)
{
    if (stage >= Stage::Args)
        BOOST_THROW_EXCEPTION(runtime_error("Only one garbage collector is possible"));
    stage = Stage::Args;

    synopsis += " [" + string(name) + "...]";
    configpaths.push_back(configpath);

    registered = pos_hide.max_total_count();
    const po::value_semantic * s =
        po::value<vector<string>>()->multitoken()->zero_tokens();
    custom.add_options()(name, s, desc);
    pos_hide.add(name, -1);

    return *this;
}
