#include <cstdlib>
#include <iostream>
#include <filesystem>
#include <memory>

#include "darwin.h"

#include <TFile.h>
#include <TChain.h>

using namespace std;

namespace fs = filesystem;

namespace DE = Darwin::Exceptions;

namespace Darwin::Tools {

////////////////////////////////////////////////////////////////////////////////
/// Get total number of entries in input ROOT file(s) or directory, and prints
/// it directly to the terminal.
void printEntries (vector<fs::path> inputs, //!< input ROOT files
                   const int steering) //!< bitfield from `Options::steering()`
try {
    inputs = GetROOTfiles(inputs);
    string location = GetFirstTreeLocation(inputs.front());
    Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree({1, 0}, location);
    cout << tIn->GetEntries() << endl;
}
catch (boost::wrapexcept<std::invalid_argument>& e) {
    cout << 0 << endl;
}

}

namespace DT = Darwin::Tools;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;

        DT::Options options("Print total number of entries of the first `TTree`s "
                            "found in any directory of the input ROOT files.");
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory");
        options(argc, argv);
        const int steering = options.steering();

        DT::printEntries(inputs, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
