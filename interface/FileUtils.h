// SPDX-License-Identifier: GPLv3-or-later
//
// SPDX-FileCopyrightText: Patrick L.S. Connor <patrick.connor@desy.de>
// SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

#pragma once

#include "exceptions.h"

#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TTree.h>

#include <filesystem>
#include <string>
#include <memory>
#include <vector>

namespace Darwin::Tools {

////////////////////////////////////////////////////////////////////////////////
/// Standard initialisation for ROOT.
///
/// "Now you own all histogram objects and you will need to delete them, for in-
/// stance through the use of `std::unique_ptr`. You can still set the directory
/// of a histogram by calling `SetDirectory()` once it has been created."
void StandardInit ();

////////////////////////////////////////////////////////////////////////////////
/// In case directories are given as inputs, instead of files, this function
/// looks for all ROOT files in those directories (and subdirectories, iteratively).
std::vector<std::filesystem::path> GetROOTfiles
        (std::vector<std::filesystem::path> inputs); //!< ROOT files or directories

////////////////////////////////////////////////////////////////////////////////
/// Load a histogram from a list of files.
template<typename THX = TH1>
[[deprecated("Use Darwin::Tools::Flow::GetInputHist[s]()")]]
inline std::unique_ptr<THX> GetHist
                (std::vector<std::filesystem::path> inputs, //!< ROOT files or directories
                 const std::string& name = "h") //!< internal path to ROOT histogram
{
    using namespace std;
    namespace fs = filesystem;

    unique_ptr<THX> sum;
    inputs = GetROOTfiles(inputs);
    for (auto const& input: inputs) {
        auto fIn = make_unique<TFile>(input.c_str(), "READ");
        /// \todo what if the file had already been opened?? will it be closed prematurely?
        unique_ptr<THX> h(fIn->Get<THX>(name.c_str()));
        if (!h) {
            namespace DE = Darwin::Exceptions;
            BOOST_THROW_EXCEPTION(DE::BadInput(Form("`%s` cannot be found in (one of) the "
                                                    " file(s).", name.c_str()), fIn));
        }
        if (sum)
            sum->Add(h.get());
        else {
            sum = std::move(h);
            sum->SetDirectory(nullptr);
        }
    }
    return sum;
}

////////////////////////////////////////////////////////////////////////////////
/// Look for a tree in the input file (stop at the first found one).
///
/// \note in principle, we take the risk of not looking at the same `TTree` as
///       all other executables, but this is unlikely in practice
std::string GetFirstTreeLocation
                (const std::filesystem::path& input); //!< one input ROOT file

////////////////////////////////////////////////////////////////////////////
/// Shortcut to create a reproducible output file (see ROOT Doxygen for details)
std::shared_ptr<TFile> GetOutputFile (const std::filesystem::path&, //!< file location
      const std::source_location = std::source_location::current()); //!< upstream function

} // namespace Darwin::Tools
