#pragma once

// STD
#include <cstdint>
#include <ios>

// Darwin
#include <GenericObject.h>

namespace Darwin::Physics {

////////////////////////////////////////////////////////////////////////////////
/// Generic generator-level muon
class GenMuon : public GenericObject {

    std::string_view scale_group () const override { return ScaleVar; }
    std::string_view weight_group () const override { return WeightVar; }

public:
    static inline const char * const ScaleVar = "GenMuonScales",
                             * const WeightVar = "GenMuonWgts";

    int Q = 0; //!< +/- 1

    GenMuon () = default;
    virtual ~GenMuon() = default;
};

////////////////////////////////////////////////////////////////////////////////
/// Generic detector-level muon
class RecMuon : public GenMuon {

    std::string_view scale_group () const override { return ScaleVar; }
    std::string_view weight_group () const override { return WeightVar; }

public:
    static inline const char * const ScaleVar = "RecMuonScales",
                             * const WeightVar = "RecMuonWgts";

    std::uint32_t selectors = 0; //!< Identification cuts

    RecMuon () = default;
    virtual ~RecMuon() = default;
};

/// \todo Electrons (just a copy of muons?)

} // end of Darwin::Physics namespace

inline std::ostream& operator<< (std::ostream& s, const Darwin::Physics::GenMuon& muon)
{
    return s << muon.CorrP4() << ' ' << muon.Q;
}
inline std::ostream& operator<< (std::ostream& s, const Darwin::Physics::RecMuon& muon)
{
    return s << muon.CorrP4() << ' ' << muon.Q << " 0x" << std::hex << muon.selectors << std::dec;
}
