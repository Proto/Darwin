#pragma once

// C
#include <cmath>

// STD
#include <algorithm>
#include <iostream>
#include <type_traits>

// ROOT
#include <Math/VectorUtil.h>

// Darwin
#include <colours.h>
#include <Variation.h>
#include <GenericObject.h>

namespace Darwin::Physics {

////////////////////////////////////////////////////////////////////////////////
/// Generic template class to store of a system of 2 objects and facilitate the
/// handling of the variations as well as the calculation of frequent variables.
///
/// `Obj1` and `Obj2` are expected to be either derivated from `GenericObjects`
/// or other specialisations of `Di<>` itself. Such nesting allows for complex
/// objects such as Z+jets or ttbar systems.
///
/// The components of the system are not stored directly but are pointers to
/// existing objects to avoid redundancy / inconsistencies and keep the object
/// light.
///
/// To obtain variations of the composite system, an instance of
/// `Darwin::Physics::Variation` may be given as argument of all relevant
/// methods. This object is passed over to the components of the system.
template<typename Obj1, typename Obj2> class Di : public std::pair<Obj1*, Obj2*>,
                                                  public AbstractGenericObject {

public:
    using std::pair<Obj1*, Obj2*>::pair;   //!< Import constructors from `std::pair`
    using std::pair<Obj1*, Obj2*>::first,  //!< First particle in the pair (field imported from `std::pair`)
          std::pair<Obj1*, Obj2*>::second; //!< Second particle in the pair (field imported from `std::pair`)

    explicit operator bool () const { return first && second; }

    inline void clear () { first = nullptr; second = nullptr; }

    inline FourVector CorrP4 (const Variation& v = nominal) const override { return first->CorrP4(v) + second->CorrP4(v); }
    inline float      CorrPt (const Variation& v = nominal) const override { return CorrP4(v).Pt(); }

    inline float Rapidity (const Variation& v = nominal) const { return CorrP4(v).Rapidity(); }
    inline float AbsRap   (const Variation& v = nominal) const { return std::abs(Rapidity(v)); }

    inline float DeltaEta (const Variation& v = nominal) const { return second->CorrP4(v).Eta() - first->CorrP4(v).Eta(); }
    inline float DeltaPhi (const Variation& v = nominal) const { return ROOT::Math::VectorUtil::DeltaPhi(first->CorrP4(v),
                                                                                                         second->CorrP4(v)); }
    inline float DeltaR   (const Variation& v = nominal) const { return ROOT::Math::VectorUtil::DeltaR  (first->CorrP4(v),
                                                                                                         second->CorrP4(v)); }

    inline float Yboost (const Variation& v = nominal) const { return (first->Rapidity(v) + second->Rapidity(v))/2; }
    inline float Ystar  (const Variation& v = nominal) const { return (first->Rapidity(v) - second->Rapidity(v))/2; }

    inline float Ymax (const Variation& v = nominal) const { return std::max(first->AbsRap(v), second->AbsRap(v)); }
    inline float HT   (const Variation& v = nominal) const { return (first->CorrPt(v) + second->CorrPt(v))/2; }

    inline double Weight (const Variation& v = nominal) const override { return first->Weight(v) * second->Weight(v); }
};

} // end of Darwin::Physics namespace

#if 0

////////////////////////////////////////////////////////////////////////////////
/// Generic overload of `operator+` to define composite objects as follows:
/// ~~~cpp
/// auto recdimuon = genmuons.at(0) + genmuons.at(1);
/// auto reczjet = recdimuon + genjets.at(0);
/// // or even
/// auto recW1 = recJet.at(1) + recJet.at(2),
///      recW2 = recJet.at(4) + recJet.at(5);
/// auto recT1 = recJet.at(0) + recW1,
///      recT2 = recJet.at(3) + recW2;
/// auto recTTbar = recT1 + recT2;
/// ~~~
template<typename Obj1, typename Obj2,
    class = typename std::enable_if_t<std::is_base_of_v<Darwin::Physics::AbstractGenericObject, Obj1>>,
    class = typename std::enable_if_t<std::is_base_of_v<Darwin::Physics::AbstractGenericObject, Obj2>>>
inline auto operator+ (Obj1& o1, Obj2& o2)
{
    return Darwin::Physics::Di<Obj1,Obj2>{&o1,&o2};
}

template<typename Obj1, typename Obj2>
std::ostream& operator<< (std::ostream& s, const Darwin::Physics::Di<Obj1,Obj2>& di)
{
    if (di)
        return s << '[' << *di.first << ", " << *di.second << "] = " << di.CorrP4();
    else
        return s << red << "invalid system" << normal;
}

#endif
