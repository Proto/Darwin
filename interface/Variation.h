#pragma once

// STD
#include <iostream>
#include <memory>
#include <string>

// Darwin
#include <Weight.h>

namespace Darwin::Physics {

enum VarType {
    single    = 0b0000,
    symmetric = 0b0001,
    replicas  = 0b0010,
    bits      = 0b0100
}; //!< types of variations

using enum VarType;

////////////////////////////////////////////////////////////////////////////////
/// Generic class to hold the relevant information to identify the variation
/// indices. This class may be passed over to physics (simple or composite)
/// objects to vary (or not) the kinematics or the weight respectively.
class Variation {

    std::string group; //!< e.g. "GenEventWgts"
    std::string name; //!< variation name (including "Up" or "Down")

    std::size_t index; //!< e.g. index in `PhysicsObject::scales`
    int bit; //!< correlation bit (needed for binned corrections with own stat. unc.)

public:

    ////////////////////////////////////////////////////////////////////////////////
    /// Destructor
    ~Variation () = default;

    ////////////////////////////////////////////////////////////////////////////////
    /// Normal constructor with C-style strings
    Variation (const char * group, const char * name,
               size_t index = 0, int bit = 0) :
        group(group), name(name), index(index), bit(bit)
    { }

    ////////////////////////////////////////////////////////////////////////////////
    /// Normal constructor with `std::string`s
    Variation (const std::string& group, const std::string& name,
               size_t index = 0, int bit = 0) :
        group(group), name(name), index(index), bit(bit)
    { }

    ////////////////////////////////////////////////////////////////////////////////
    /// Copy constructor
    Variation (const Variation& v) :
        group(v.group), name(v.name), index(v.index), bit(v.bit)
    { }

    ////////////////////////////////////////////////////////////////////////////////
    /// Move constructor
    Variation (Variation&& v) :
        group(std::move(v.group)), name(std::move(v.name)), index(v.index), bit(v.bit)
    { }

    inline std::string_view Group () const { return group; }
    inline std::string_view Name  () const { return name ; }
    inline size_t           Index () const { return index; }
    inline int              Bit   () const { return bit  ; }
};

inline const Variation nominal("", "nominal");

std::strong_ordering operator<=> (const Variation& lhs, const Variation& rhs);

} // end of namespace Darwin::Physics

std::ostream& operator<< (std::ostream& s, const Darwin::Physics::Variation& v);
