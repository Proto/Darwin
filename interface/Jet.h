#pragma once

// STD
#include <cstdint>
#include <ios>

// Darwin
#include <GenericObject.h>

namespace Darwin::Physics {

////////////////////////////////////////////////////////////////////////////////
/// Generic generator-level jet
class GenJet : public GenericObject {

    std::string_view scale_group () const override { return ScaleVar; }
    std::string_view weight_group () const override { return WeightVar; }

public:
    static inline const char * const ScaleVar = "GenJetScales",
                             * const WeightVar = "GenJetWgts";

    int nBmesons = -1, //!< Number of *B* hadrons
        nDmesons = -1; //!< Number of *D* hadrons
    int partonFlavour = 0; //!< Parton flavour (PDG ID)

    GenJet () = default;
    virtual ~GenJet () = default;
};

////////////////////////////////////////////////////////////////////////////////
/// Generic detector-level jet
class RecJet : public GenJet {

    std::string_view scale_group () const override { return ScaleVar; }
    std::string_view weight_group () const override { return WeightVar; }

public:
    static inline const char * const ScaleVar = "RecJetScales",
                             * const WeightVar = "RecJetWgts";

    float area = M_PI*0.4*0.4; //!< jet area (default value corresponds to average AK4 jet)
    std::uint32_t selectors = 0; //!< Identification cuts (e.g. JetID at CMS)

    RecJet () = default;
    virtual ~RecJet () = default;
};

} // end of Darwin::Physics namespace

inline std::ostream& operator<< (std::ostream& s, const Darwin::Physics::GenJet& jet)
{
    return s << jet.CorrP4() << ' ' << jet.nDmesons << ' ' << jet.nBmesons;
}

inline std::ostream& operator<< (std::ostream& s, const Darwin::Physics::RecJet& jet)
{
    return s << jet.CorrP4() << " 0x" << std::hex << jet.selectors << std::dec;
}
