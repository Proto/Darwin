#ifndef DARWIN_USERINFO_H
#define DARWIN_USERINFO_H

#include <cassert>

#include <type_traits>
#include <iostream>
#include <filesystem>
#include <typeinfo>
#include <stdexcept>
#include <string>
#include <functional>
#include <sstream>

#include <TList.h>
#include <TString.h>
#include <TParameter.h>
#include <TObjString.h>

#include <boost/property_tree/ptree.hpp>      
#include <boost/exception/all.hpp>

class TTree;

namespace Darwin::Tools {

////////////////////////////////////////////////////////////////////////////////
/// \brief Generic meta-information for n-tuple (can be used out of Darwin).
///
/// Generic class to read the meta-information contained in a list.
/// The original goal is to read and write information to store in the list
/// `TTree::GetUserInfo()`, but it can be used in general.
///
/// The elements of the list may simply be reached with `UserInfo::Get()`
/// and written with `UserInfo::Set()`. The list may contain simple types
/// such as arithmetic types or literals as well as other lists.
/// To reach a list, the `Get` and `Set` function simply need the list of 
/// keys to navigate through the tree structure, provided with additional 
/// arguments. Examples:
/// ~~~cpp
/// UserInfo ui;
/// ui.Set<bool>("one", "two", "myBool", true);
/// cout << ui.Get<bool>("one", "two", "myBool") << endl; // will print true
/// cout << ui.Get<string>("this", "will", "fail") << endl;
/// ~~~
/// The whole information may be extracted in JSON, INFO, or XML format with
/// `Darwin::Tools::UserInfo::Write()`.
///
/// This class is as generic as can be. In certain cases, one may want to 
/// use inherit from this class to enforce a more rigid structure. One example
/// is `Darwin::Tools::MetaInfo`.
class UserInfo {
public:
    ////////////////////////////////////////////////////////////////////////////////
    /// Supported serialization formats
    enum Format {
        INFO, //!< INFO format, proper to Boost Property Trees (default)
        JSON, //!< JSON format, rather common and nearly as powerful
        XML //!< XML format, another universal format
    };

private:
    bool own;
    TList * l; //!< main list (typically from `TTree::GetUserInfo()`)

    ////////////////////////////////////////////////////////////////////////////////
    /// Accest do daughter list from a mother list
    TList * List (TList * mother, const char * key) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Final getter. The behaviour for arithmetic types and for literal (default) 
    /// is slightly different, since literals don't really have a key.
    /// Literals are always contained in a list, and the last element of the list
    /// is returned.
    template<class T> T Get (TList * mother, const char * key) const
    {
        if constexpr (std::is_arithmetic_v<T>) {
            TObject * obj = mother->FindObject(key);
            auto parameter = dynamic_cast<TParameter<T>*>(obj);
            if (parameter == nullptr)
                BOOST_THROW_EXCEPTION(std::invalid_argument(
                        Form("No `TParameter` with name '%s' could be found in the `TList` '%s'.", key, mother->GetName())));
            return parameter->GetVal();
        }
        else {
            TList * daughter = List(mother, key);
            TIter next(daughter->begin());
            auto objs = dynamic_cast<TObjString*>(daughter->Last());
            if (daughter->GetSize() == 0 || objs == nullptr)
                BOOST_THROW_EXCEPTION(std::invalid_argument(
                        Form("No literal with name '%s' was found in the `TList` '%s'.", key, mother->GetName())));
            T literal = objs->GetString().Data();
            return literal;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Recursive getter
    template<class T, typename... Args> T Get (TList * mother, const char * key, Args... args) const
    {
        TList * daughter = List(mother, key);
        return Get<T>(daughter, args...);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Recursive finder
    bool Find (TList * mother, const char * key) const
    {
        TObject * obj = mother->FindObject(key);
        return (obj != nullptr);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Recursive finder
    template<typename... Args> bool Find (TList * mother, const char * key, Args... args) const
    {
        if (!Find(mother, key)) return false;
        TList * daughter = List(mother, key);
        if (daughter->GetSize() > 0)
            return Find(daughter, args...);
        else return false;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// (Re)sets elements of a given list.
    ///
    /// Only arithmetic types have a key and a value.
    /// Literals only have identical key and value.
    template<class T> void Set (TList * mother, const char * key, T value) const
    {
        if constexpr (std::is_arithmetic_v<T>) {
            TObject * obj = mother->FindObject(key);
            if (obj) {
                auto parameter = dynamic_cast<TParameter<T>*>(obj);
                if (parameter == nullptr)
                    BOOST_THROW_EXCEPTION(std::invalid_argument(
                                Form("No `TParameter` with name '%s' and typeid '%s' was found in the `TList` '%s'.",
                                                key, typeid(T).name(), mother->GetName())));
                parameter->SetVal(value);
            }
            else {
                auto parameter = new TParameter<T>(key, value, 'l');
                mother->Add(parameter);
            }
        }
        else {
            TList * daughter = List(mother, key);
            TString str(value);
            auto objs = new TObjString(str);
            daughter->Add(objs);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Recursive setter
    template<class T, typename... Args> void Set (TList * mother, const char * key, Args... args) const
    {
        TList * daughter = List(mother, key);
        Set<T>(daughter, args...);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Access a sublist
    template<typename... Args> TList * List (TList * mother, const char * key, Args... args) const
    {
        TList * daughter = List(mother, key);
        return List(daughter, args...);
    }

    template<class T>
    inline T       Get  (TList * mother, std::string key) const { return Get<T>(mother, key.c_str()); } //!< value getter with string
    inline bool    Find (TList * mother, std::string key) const { return Find  (mother, key.c_str()); } //!< finder with string
    inline TList * List (TList * mother, std::string key) const { return List  (mother, key.c_str()); } //!< list getter with string

    ////////////////////////////////////////////////////////////////////////////////
    /// Helper function to convert a ptree to a TList
    /// Supports making strings, bools, ints, and floats.
    void ConvertPtree (const boost::property_tree::ptree&, const std::string&, TList*);

protected:
    
    ////////////////////////////////////////////////////////////////////////////////
    /// Write `TList` to Boost Property Tree. Calls itself recursively.
    /// Internally, it uses Boost Preprocessor macros to loop over types.
    ///
    /// \return content of `TList` given in argument in Property Tree format
    boost::property_tree::ptree MkPtree (TList *, //!< list to write to `ptree`
                 Format = INFO //!< format of output file (e.g. XML cannot handle empty keys)
                 ) const;

public:
    ////////////////////////////////////////////////////////////////////////////////
    /// Access directly a sublist
    template<typename... Args> TList * List (const char * key, Args... args) const
    {
        return List(l, key, args...);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Access directly to main list
    inline TList * List () const
    {
        return l;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Generic getter
    template<class T, typename... Args> T Get (const char * key, Args... args) const
    {
        return Get<T>(l, key, args...);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Generic finder
    template<typename... Args> bool Find (const char * key, Args... args) const
    {
        return Find(l, key, args...);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Generic setter. The key is the name of the first sublist (if any), or to 
    /// the element directly. An extra argument (implicitly in the variadic argument)
    /// contains the value.
    template<class T, typename... Args> void Set (const char * key, Args... args) const
    {
        Set<T>(l, key, args...);
    }

    UserInfo (const char * = "UserInfo"); //!< Constructor (starting from scratch)
    UserInfo (const boost::property_tree::ptree&); //!< Constructor (starting from existing ptree)
    UserInfo (TList *); //!< Constructor (starting from existing list)
    UserInfo (TTree *); //!< Constructor (starting from TTree)
    UserInfo (TTree *, const boost::property_tree::ptree&); //!< constructor (inserting existing ptree direcrly in a ttree)
    ~UserInfo (); //!< Destructor

    void Write () const; //!< Write `TList` to `TFile` in ROOT format
    bool Empty () const; //!< Check if list is empty

    ////////////////////////////////////////////////////////////////////////////////
    /// Write to stream with Boost. Calls `MkPtree` to write sublists.
    ///
    /// \return content of full `TList` in Property Tree format
    boost::property_tree::ptree Write (std::ostream&, //!< output stream
                                       Format = INFO  //!< format of output
                                       ) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Write to file with Boost. Calls `MkPtree` to write sublists.
    ///
    /// \return content of full `TList` in Property Tree format
    boost::property_tree::ptree Write (const std::filesystem::path& //!< path to output
                                       ) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Write main `TList` to Boost Property Tree.
    ///
    /// \return content of full `TList` in Property Tree format
    inline boost::property_tree::ptree MkPtree (Format format = INFO //!< format of output file
                                ) const { return MkPtree(l, format); }

    ////////////////////////////////////////////////////////////////////////////////
    /// Prints the content of the `TList`
    inline void ls () const { if (l != nullptr) l->ls(); }
};

} // end of namespace

////////////////////////////////////////////////////////////////////////////////
/// Hash from property tree by converting to string.
template<> struct std::hash<boost::property_tree::ptree> {
    std::size_t operator() (boost::property_tree::ptree const&) const noexcept;
};

////////////////////////////////////////////////////////////////////////////////
/// Hash from UserInfo by converting to property tree.
template<> struct std::hash<Darwin::Tools::UserInfo> {
    std::size_t operator() (Darwin::Tools::UserInfo const&) const noexcept;
};

////////////////////////////////////////////////////////////////////////////////
/// \brief Hash from TTree obtained by combination of different elements.
///
/// \remark The events themselves are not used, as it would take too long. 
///         Therefore, the hash may be identical for two `TTree`s only differeing
///         slightly event by event.
template<> struct std::hash<TTree *> {
    std::size_t operator() (TTree * const&) const noexcept;
};

#endif 
