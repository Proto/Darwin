#pragma once

// STD
#include <vector>

// Darwin
#include <Variation.h>
#include <Weight.h>

// ROOT
#include <TRandom3.h>

namespace Darwin::Physics {

////////////////////////////////////////////////////////////////////////////////
/// Generic event class for both generator and detector levels. Most notably,
/// it provides access to the weights via `Darwin::Physics::Variation`.
class AbstractEvent {
protected:
    virtual std::string_view weight_group () const = 0;

    AbstractEvent () = default;
    virtual ~AbstractEvent () = default;

public:
    Weights weights = {{1.,0}}; //!< e.g. cross section normalisation

    inline double Weight (const Variation& v) const //!< weight
    {
        if (v.Group() == weight_group()) {
            namespace DP = Darwin::Physics;
            const DP::Weight& w = weights.at(v.Index());
            if (w.i == v.Bit()) return w;
        }
        return weights.front();
    }

    virtual void clear () = 0;
};

////////////////////////////////////////////////////////////////////////////////
/// Generic generator-level event
class GenEvent : public AbstractEvent {

    std::string_view weight_group () const override { return WeightVar; }

public:
    static inline const char * const WeightVar = "GenEvtWgts"; //< Name of gen weights in variations

    float hard_scale = 0; //!< hard scale, corresponding to `pthat` in Pythia 8

    GenEvent () = default;
    virtual ~GenEvent () = default;

    void clear () override //!< to clear for each new event in *n*-tupliser
    {
        hard_scale = 0;
        weights = {{1.,0}};
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Generic detector-level event
class RecEvent : public AbstractEvent {

    std::string_view weight_group () const override { return WeightVar; }

public:
    static inline const char * const WeightVar = "RecEvtWgts"; //< Name of rec weights in variations

    int fill = 0,  //!< at LHC, typically a 4-digit number
        runNo = 0, //!< at LHC, typically a 6-digit run number
        lumi = 0;  //!< lumi section (shortest unit time in LHC jargon)
    unsigned long long evtNo = 0; //!< event number

    RecEvent () = default;
    virtual ~RecEvent () = default;

    void clear () override //!< to clear for each new event in *n*-tupliser
    {
        fill = 0;
        runNo = 0;
        lumi = 0;
        evtNo = 0;
        weights = {{1.,0}};
    }
};

} // end of Darwin::Physics

inline std::ostream& operator<< (std::ostream& s, const Darwin::Physics::GenEvent& event)
{
    return s << event.hard_scale;
}

inline std::ostream& operator<< (std::ostream& s, const Darwin::Physics::RecEvent& event)
{
    return s << event.fill << ' ' << event.runNo << ' ' << event.lumi << ' ' << event.evtNo;
}
