#pragma once

/**
 * \file Includes all of Darwin.
 */

#include "colours.h"
#include "exceptions.h"
#include "FileUtils.h"
#include "Flow.h"
#include "FriendUtils.h"
#include "MetaInfo.h"
#include "Options.h"
#include "Looper.h"

// Doxygen for namespaces.

////////////////////////////////////////////////////////////////////////////////
/// Global namespace for libraries and executables for analysis with Darwin.
namespace Darwin {

    ////////////////////////////////////////////////////////////////////////////////
    /// Everything what concerns physics analysis directly.
    namespace Physics {
        // nihil
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Classes and functions related to the framework.
    namespace Tools {
        // nihil
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Handling of exceptions.
    namespace Exceptions {
        // nihil
    }

}

////////////////////////////////////////////////////////////////////////////////
/// [Manual](https://root.cern/manual) [Doxygen](https://root.cern.ch/doc/master)
namespace ROOT {
    // nihil
}
