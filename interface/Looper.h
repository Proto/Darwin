#pragma once

#include <algorithm>
#include <memory>
#include <iostream>
#include <string>

#include <chrono>
#include <ctime>

#include <boost/exception/all.hpp>

#include <TChain.h>
#include <TTree.h>

#include "exceptions.h"

namespace Darwin::Tools {

////////////////////////////////////////////////////////////////////////////////
/// Facility to loop over a n-tuple, including parallelisation and printing.
class Looper {
    TTree * tree; //!< tree

    long long nEv, // number of entries covered by the current process
              iEv, // current entry
              percent; // percentage of progress for current entry

    std::chrono::time_point<std::chrono::system_clock> start_t; //!< starting time

    Looper (TTree * t, //!< n-tuple
            long long nEvents //!< total number of events to generate
            ) : tree(t)
              , nEv(nEvents)
              , iEv(0)
              , percent(-1)
              , start_t(std::chrono::system_clock::now())
    {
        using namespace std;

        if (nEv <= 0)
            BOOST_THROW_EXCEPTION(
                    invalid_argument("The number of events must be nonnegative"));
    }

public:

    static long long division; //!< steps at which progress is printed (100%/division) 

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor for existing tree with raw pointer
    template<typename TTreePtr>
    Looper (const TTreePtr& t //!< n-tuple
            ) : Looper(&*t, t->GetEntries())
    {
        using namespace std;
        namespace DE = Darwin::Exceptions;
        string root_log = DE::intercept_printf([this]() {
                    printf("loading first entry");
                    tree->GetEntry(0);
                });
        if (root_log.find("Error") != string::npos)
            BOOST_THROW_EXCEPTION(runtime_error("Error while loading a TTree entry:\n" + root_log));
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor for simple counter
    Looper (long long nEvents //!< total number of events to generate
            ) : Looper(nullptr, nEvents)
    {
    }

    ~Looper ()
    {
        if (percent < 100ll)
            std::cerr << red << "Warning: the event loop has stopped at entry "
                      << iEv << def << '\n';
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Increments the counter and load entry (if applicable)
    void operator++ ()
    {
        ++iEv;
        if (tree) tree->GetEntry(iEv);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Check that the counter is still in the range and prints percentage
    bool operator() ()
    {
        using namespace std;
        long long test_percent = (100ll*iEv)/nEv;

        if (test_percent != percent && test_percent % division == 0) {

            // display total number of seconds
            auto now_t = chrono::system_clock::now();
            auto elapsed_time {now_t - start_t};
            auto secs = chrono::duration_cast<chrono::seconds>(elapsed_time);
            cout << secs.count() << 's';

            // display current time in human readable format
            time_t now = chrono::system_clock::to_time_t(now_t);
            cout << '\t' << put_time(localtime(&now), "%Y-%m-%d %H:%M:%S %Z");

            // display progress
            percent = test_percent;
            cout << '\t' << percent << '%' << endl;
        }

        return iEv < nEv;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Pointer-like operator to return the value of the entry
    inline long long operator* () const
    {
        return iEv;
    }
};

} // end of Darwin::Tools namespace
