# SPDX-License-Identifier: GPLv3-or-later
#
# SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

#[===[.rst:
CMSSWToolchain
--------------

This toolchain file adds support for using CMake in a CMSSW environment. When
using this toolchain, the ``find_package`` command is overriden to use
``scram`` for a few selected modules:

* ``Boost`` (using the native ``scram`` configuration)
* ``libgit2`` (through ``pkg-config``)
* ``ROOT`` (using the native ``ROOTConfig`` CMake configuration)

This is achieved by pointing the normal ``find_package`` function at the right
location and should be fully transparent for the user.
#]===]

# Prevent multiple inclusion
if (CMSSW_TOOLCHAIN)
    return()
endif()
set(CMSSW_TOOLCHAIN ON)

if (NOT DEFINED ENV{CMSSW_BASE})
    message(FATAL_ERROR "A CMS environment is required to use this toolchain. Run cmsenv.")
endif()

# Extracts a configuration variable from ``scram``. The variable is kept in
# cache so ``scram`` doesn't have to be run every time.
function(_scram_get_variable module_name tool variable)
    set(output_variable ${module_name}_SCRAM_${variable})
    if (NOT DEFINED ${output_variable})
        execute_process(COMMAND scram tool tag ${tool} ${variable}
                        WORKING_DIRECTORY "$ENV{CMSSW_BASE}"
                        OUTPUT_VARIABLE result
                        OUTPUT_STRIP_TRAILING_WHITESPACE)

        set(${output_variable} "${result}" CACHE PATH
            "${module_name} ${variable} variable obtained from scram")
        mark_as_advanced(${output_variable})
    endif()
endfunction()

# Finds a package using scram
macro(find_package _scram_name)
    if (${_scram_name} STREQUAL Boost)
        # We set INCLUDEDIR, the find module does the rest
        _scram_get_variable(Boost boost_header INCLUDE)
        set(BOOST_INCLUDEDIR "${Boost_SCRAM_INCLUDE}")

    elseif (${_scram_name} STREQUAL LibGit2)
        # The find module uses pkg-config. We set the correct path for that
        _scram_get_variable(LibGit2 libgit2 LIBDIR)
        set(_scram_old_cmake_prefix_path "${CMAKE_PREFIX_PATH}")
        set(CMAKE_PREFIX_PATH "${LibGit2_SCRAM_LIBDIR}/..")
        _find_package(${_scram_name} ${ARGN} QUIET)
        set(CMAKE_PREFIX_PATH "${_scram_old_cmake_prefix_path}")

    elseif (${_scram_name} STREQUAL ROOT)
        # We hint CMake at the location of ROOTConfig.cmake
        _scram_get_variable(ROOT root_interface ROOT_INTERFACE_BASE)
        set(ROOT_ROOT "${ROOT_SCRAM_ROOT_INTERFACE_BASE}/cmake")
    endif()

    _find_package(${_scram_name} ${ARGN})
endmacro()

