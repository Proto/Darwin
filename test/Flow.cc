#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define BOOST_TEST_MODULE Flow

#include <boost/test/included/unit_test.hpp>
#include <boost/exception/all.hpp>

#include "darwin.h"
#include "Event.h"
#include "test.h"

#include <TFile.h>

using namespace std;
namespace fs = std::filesystem;

using namespace Darwin::Tools;
namespace DP = Darwin::Physics;
namespace DE = Darwin::Exceptions;

namespace po = boost::program_options;
namespace pt = boost::property_tree;

using boost::wrapexcept;

struct EventBoom : public DP::RecEvent {
    int i;
};

BOOST_AUTO_TEST_SUITE( branch_setters )

    BOOST_AUTO_TEST_CASE( write_only )
    {
        Flow flow(verbose | Darwin::Tools::fill);

        BOOST_TEST_MESSAGE( "Trying to get a branch before the input tree has been set up" );
        BOOST_REQUIRE_THROW( flow.GetBranchWriteOnly<DP::RecEvent>("recEvent"), wrapexcept<invalid_argument> );

        BOOST_TEST_MESSAGE( "Testing the unicity of the output tree" );
        auto tOut1 = flow.GetOutputTree("flow.root"),
             tOut2 = flow.GetOutputTree();
        BOOST_TEST( tOut1 == tOut2 );

        BOOST_TEST_MESSAGE( "Trying to write twice to the same branch." );
        DP::RecEvent * ev = nullptr,
                     * ev2 = nullptr;
        BOOST_REQUIRE_NO_THROW( ev = flow.GetBranchWriteOnly<DP::RecEvent>("recEvent") );
        BOOST_REQUIRE_NO_THROW( ev2 = flow.GetBranchWriteOnly<DP::RecEvent>("recEvent") );
        BOOST_TEST( ev == ev2 );

        BOOST_TEST_MESSAGE( "Trying to write to a branch with a missing dictionary." );
        BOOST_REQUIRE_THROW( flow.GetBranchWriteOnly<EventBoom>("recEventBOOM"), wrapexcept<DE::BadInput> );

        ev->fill = 1234;
        ev->runNo = 424242;
        ev->lumi = 123456789;
        tOut1->Fill();
        BOOST_TEST( tOut1->GetEntries() > 0 );
    }

    BOOST_AUTO_TEST_CASE( read_only_getter )
    {
        Flow flow(verbose, {"flow.root"});

        BOOST_TEST_MESSAGE( "Trying to get the input tree before it has been set up" );
        BOOST_REQUIRE_THROW( flow.GetBranchReadOnly<DP::RecEvent>("recEvent"), wrapexcept<invalid_argument> );
        BOOST_REQUIRE_THROW( flow.GetInputTree({0, 0}), wrapexcept<invalid_argument> );
        BOOST_REQUIRE_THROW( flow.GetInputTree({0, 1}), wrapexcept<invalid_argument> );

        auto tIn = flow.GetInputTree();
        BOOST_TEST( tIn->GetEntries() > 0 );
        tIn->Show(0);

        BOOST_TEST_MESSAGE( "Trying to read an existing branch with the correct type" );
        BOOST_REQUIRE_NO_THROW( flow.GetBranchReadOnly<DP::RecEvent>("recEvent") );

        BOOST_TEST_MESSAGE( "Trying to read inexisting branch" );
        BOOST_REQUIRE_THROW( flow.GetBranchReadOnly<DP::RecEvent>("recEvent2"), wrapexcept<DE::BadInput> );

        BOOST_TEST_MESSAGE( "Trying to read an existing branch assuming a wrong type" );
        BOOST_REQUIRE_THROW( flow.GetBranchReadOnly<DP::GenEvent>("recEvent"), wrapexcept<bad_any_cast> );

        BOOST_TEST_MESSAGE( "Trying to get output file when it has not been set" );
        BOOST_TEST( flow.GetOutputFile() == nullptr );

        BOOST_TEST_MESSAGE( "Setting custom output file" );
        auto fOut = make_shared<TFile>("flow2.root", "RECREATE");
        BOOST_REQUIRE_NO_THROW( flow.SetOutputFile(fOut) );
        BOOST_TEST( flow.GetOutputFile() != nullptr );
    }

    BOOST_AUTO_TEST_CASE( read_only_applier )
    {
        Flow flow(verbose | Friend, {"flow.root"});
        auto tIn = flow.GetInputTree();
        tIn->GetEntry(0);
        BOOST_TEST( tIn->GetTree()->GetTitle() == "Flow"s );

        BOOST_TEST_MESSAGE( "Forcing same command for input and output tree" );
        auto tOut = flow.GetOutputTree("flow2.root");

        BOOST_TEST_MESSAGE( "Trying to access the same branch once in read-only mode and once in write-only" );
        DP::RecEvent * ev = nullptr,
                     * ev2 = nullptr;
        BOOST_REQUIRE_NO_THROW( ev = flow.GetBranchReadOnly<DP::RecEvent>("recEvent") );
        BOOST_REQUIRE_NO_THROW( ev2 = flow.GetBranchWriteOnly<DP::RecEvent>("recEvent") );
        BOOST_TEST( ev == ev2 );

        ev2->fill = 1235;
        ev2->runNo = 424243;
        ev2->lumi = 123456790;
        BOOST_TEST( tOut->Fill() > 0 );
        tOut->Show(0);
    }

    BOOST_AUTO_TEST_CASE( read_write )
    {
        int steering1 = verbose,
            steering2 = verbose | Friend;
        for (int steering: {steering1, steering2}) {
            Flow flow(steering, {"flow2.root"});

            BOOST_TEST_MESSAGE( "Trying to get a branch before the input has been set up" );
            BOOST_REQUIRE_THROW( flow.GetBranchReadWrite<DP::RecEvent>("recEvent"), wrapexcept<invalid_argument> );
            auto tIn = flow.GetInputTree();
            tIn->Show(0);

            BOOST_TEST_MESSAGE( "Trying to get a branch before the output has been set up" );
            BOOST_REQUIRE_THROW( flow.GetBranchReadWrite<DP::RecEvent>("recEvent"), wrapexcept<invalid_argument> );

            BOOST_TEST_MESSAGE( "Testing default name in case of friends" );
            tIn->GetTree()->SetTitle("");
            auto t1 = flow.GetOutputTree();

            BOOST_TEST_MESSAGE( "Trying to read an existant branch with the correct type" );
            BOOST_REQUIRE_NO_THROW( flow.GetBranchReadWrite<DP::RecEvent>("recEvent") );

            BOOST_TEST_MESSAGE( "Trying to read an inexistant branch" );
            BOOST_REQUIRE_THROW( flow.GetBranchReadWrite<DP::RecEvent>("recEvent2"), wrapexcept<DE::BadInput> );
            BOOST_TEST( flow.GetBranchReadWrite<DP::RecEvent>("recEvent2", facultative) == nullptr );

            BOOST_TEST_MESSAGE( "Trying to write to an existing branch assuming a wrong type" );
            BOOST_REQUIRE_THROW( flow.GetBranchReadWrite<DP::GenEvent>("recEvent"), wrapexcept<bad_any_cast> );

            BOOST_TEST_MESSAGE( "Trying to write to a branch with a missing dictionary." );
            BOOST_REQUIRE_THROW( flow.GetBranchReadWrite<EventBoom>("recEventBOOM"), wrapexcept<DE::BadInput> );

            BOOST_TEST_MESSAGE( "Trying to write twice to the same branch." );
            DP::RecEvent * ev = nullptr,
                         * ev2 = nullptr;
            BOOST_REQUIRE_NO_THROW( ev = flow.GetBranchReadWrite<DP::RecEvent>("recEvent") );
            BOOST_REQUIRE_NO_THROW( ev2 = flow.GetBranchReadWrite<DP::RecEvent>("recEvent") );
            BOOST_TEST( ev == ev2 );

            t1->GetEntry(0);
            BOOST_TEST( ev == ev2 );
        }
    }

    BOOST_AUTO_TEST_CASE( hists )
    {
        {
            auto f = make_unique<TFile>("flow_hist.root", "RECREATE");
            auto h = make_unique<TH1D>("h", "", 1, 0, 1);
            h->SetBinContent(1, 42);
            h->Write("h1");
            h->Write("h2");
        }

        Flow flow(verbose, {"flow_hist.root"});
        const char * h1s = "h1";
        string h2s = "h2";
        auto [h1, h2] = flow.GetInputHists(h1s, h2s);
        BOOST_TEST( h1->GetBinContent(1) == 42 );
        BOOST_TEST( h2->GetBinContent(1) == 42 );
    }

BOOST_AUTO_TEST_SUITE_END()

#endif
