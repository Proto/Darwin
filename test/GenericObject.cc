#ifndef DOXYGEN_SHOULD_SKIP_THIS

// Darwin
#include <GenericObject.h>

// BOOST
#define BOOST_TEST_MODULE GenericObject
#include <boost/test/included/unit_test.hpp>

// STD
#include <iostream>

using namespace Darwin::Physics;
using namespace std;

namespace utf = boost::unit_test;

class GenericObjectDummy : public GenericObject {
    std::string_view scale_group () const override { return ScaleVar; }
    std::string_view weight_group () const override { return WeightVar; }
public:
    static inline const char * const ScaleVar = "DummyScales",
                             * const WeightVar = "DummyWgts";
    GenericObjectDummy () = default;
    ~GenericObjectDummy () = default;
};

BOOST_AUTO_TEST_CASE( scale )
{
    GenericObjectDummy obj;
    BOOST_TEST( obj.scales.size() == 1 );
    BOOST_TEST( obj.scales.front() == 1. );

    obj.p4 = {1,1,1,1};
    cout << obj << endl;

    obj.scales.front() = 2;
    BOOST_TEST( obj.scales.front() == 2. );
    cout << obj << endl;

    FourVector corr = obj.CorrP4();
    BOOST_TEST( obj.p4. Pt()*2 == corr.Pt() );
    BOOST_TEST( obj.CorrPt()   == corr.Pt() );

    BOOST_TEST( obj.Rapidity() == corr.Rapidity() );
    BOOST_TEST( obj.p4.Eta  () == corr.Eta     () );
}

BOOST_AUTO_TEST_CASE( weights )
{
    GenericObjectDummy obj;
    BOOST_TEST( obj.weights.size() == 1 );
    BOOST_TEST( obj.weights[0] == 1 );
}

BOOST_AUTO_TEST_CASE( comparisons, * utf::tolerance(0.00001 ) )
{
    GenericObjectDummy obj1, obj2;
    obj1.p4.SetCoordinates(2540.7522, -0.939323, 2.9671194, 42.679748);
    obj2.p4.SetCoordinates(2524.7915, 0.3277488, -0.195445, 58.079277);

    BOOST_TEST( obj1 == obj1 );
    BOOST_TEST( obj2 < obj1 );
    BOOST_TEST( obj1 > obj2 );
}

#endif
