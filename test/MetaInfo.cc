#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define BOOST_TEST_MODULE testMetaInfo
#include <boost/test/included/unit_test.hpp>
#include <boost/exception/all.hpp>

#include "test.h"
#include "MetaInfo.h"

#include <TFile.h>
#include <TTree.h>

#include <memory>

#include <boost/property_tree/ptree.hpp>      
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>

using namespace std;
namespace fs = std::filesystem;

namespace pt = boost::property_tree;

using namespace Darwin::Tools;

BOOST_AUTO_TEST_SUITE( meta_info )

    BOOST_AUTO_TEST_CASE( flags_and_corrections )
    {
        pt::ptree config;
        BOOST_REQUIRE_NO_THROW( pt::read_info(DARWIN "/test/example.info", config) );

        auto events = make_shared<TTree>("events", "events");

        BOOST_REQUIRE_THROW( MetaInfo(events, config.get_child("corrections")), boost::wrapexcept<pt::ptree_error> ); // expecting a certain format

        MetaInfo metainfo1(events, config);
        BOOST_TEST( metainfo1.Get<bool>("flags", "isMC") == true );
        BOOST_TEST_WARN( metainfo1.Get<bool>("git", "reproducible") == true );
        events->GetUserInfo()->ls();
        BOOST_REQUIRE_THROW( MetaInfo(events, config), boost::wrapexcept<std::invalid_argument> ); // cannot initialise (from scratch) again the same metainfo

        config.put<bool>("flags.isMC", false);
        config.put<bool>("flags.label", "aNewLabel");
        MetaInfo metainfo2(events);
        BOOST_REQUIRE_THROW( metainfo2.Check(config), boost::wrapexcept<std::invalid_argument> ); // inconsistent flags
        hash<UserInfo> HashM;
        BOOST_TEST( HashM(metainfo1) == HashM(metainfo2) );

        auto events2 = make_shared<TTree>("events2", "events");
        MetaInfo metainfo3(events2, config);
        BOOST_TEST( HashM(metainfo1) != HashM(metainfo3) );
        hash<TTree*> HashT;
        BOOST_TEST( HashT(events.get()) != HashT(events2.get()));
    }

    BOOST_AUTO_TEST_CASE( labels )
    {
        pt::ptree config;
        pt::read_info(DARWIN "/test/example.info", config);

        {
            auto events = make_shared<TTree>("events", "events");
            config.get_child("flags").erase("labels");
            config.put<string>("flags.labels.this", ""); // will not fail
            BOOST_REQUIRE_NO_THROW( MetaInfo(events, config) );
        }

        {
            auto events = make_shared<TTree>("events", "events");
            config.get_child("flags").erase("labels");
            config.put<string>("flags.labels.this", "will fail");
            BOOST_REQUIRE_THROW( MetaInfo(events, config), boost::wrapexcept<std::invalid_argument> );
        }
    }

    BOOST_AUTO_TEST_CASE( software_version )
    {
        pt::ptree config;
        pt::read_info(DARWIN "/test/example.info", config);
        auto events = make_shared<TTree>("events", "events");
        MetaInfo metainfo(events, config);
        cout << "Forcing inconsistent version (42 everywhere)" << endl;
        metainfo.Set<const char *>("software", "gpp"  , "42");
        metainfo.Set<const char *>("software", "Cpp"  , "42");
        metainfo.Set<const char *>("software", "ROOT" , "42");
        metainfo.Set<const char *>("software", "Boost", "42");
        BOOST_REQUIRE_NO_THROW( MetaInfo{events} );
    }

BOOST_AUTO_TEST_SUITE_END()
#endif
