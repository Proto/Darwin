# Tests

## Libraries

Each library comes with a test file along, with the same name. They should be at each compilation to ensure that the workflow is not broken.

## Workflow

N-tuples are created (e.g. `example01`) then modified (`example02`) and finally projected (`example04`). Only the necessary branches should be loaded for each operation.
