#ifndef DOXYGEN_SHOULD_SKIP_THIS

// Darwin
#include <Weight.h>

#define BOOST_TEST_MODULE Weight
#include <boost/test/included/unit_test.hpp>

using namespace Darwin::Physics;
using namespace std;

BOOST_AUTO_TEST_CASE( weight )
{
    Weight weight;
    BOOST_TEST( weight == 1 );
    BOOST_TEST( weight == weight.v );
    BOOST_TEST( weight.i == 0 );

    weight *= 2;
    BOOST_TEST( weight == 2 );
    BOOST_TEST( weight.i == 0 );

    weight = 3;
    BOOST_TEST( weight == 3 );
    BOOST_TEST( weight.i == 0 );

    Weight another_weight = {2,0};
    BOOST_TEST( weight * another_weight == 6 ); }

BOOST_AUTO_TEST_CASE( weights )
{
    Weights weights {{1.,0}, {2.,0}, {3.,0}};

    weights *= 2;
    BOOST_TEST( weights[0] == 2 );
    BOOST_TEST( weights[1] == 4 );
    BOOST_TEST( weights[2] == 6 );

    Weights copy_weights = weights;
    BOOST_TEST( weights == copy_weights );
    copy_weights.back().i = 1;
    BOOST_TEST( weights != copy_weights );
}

#endif
