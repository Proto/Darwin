#ifndef DOXYGEN_SHOULD_SKIP_THIS

// Darwin
#include <Event.h>
#include <Jet.h>
#include <MetaInfo.h>
#include <Variation.h>

// ROOT
#include <TList.h>
#include <TObjString.h>

#define BOOST_TEST_MODULE Variation
#include <boost/test/included/unit_test.hpp>
#include <boost/exception/all.hpp>

using namespace Darwin::Physics;
using namespace std;
using boost::wrapexcept;

struct MyMetaInfo {
    Darwin::Tools::MetaInfo::IKnowWhatIAmDoing iknowwhatiamdoing;
    Darwin::Tools::MetaInfo metainfo;
    MyMetaInfo () : metainfo(iknowwhatiamdoing) { }
};

BOOST_FIXTURE_TEST_SUITE( variations, MyMetaInfo )

    BOOST_AUTO_TEST_CASE( model_like )
    {
        auto models = metainfo.AddVars(GenEvent::WeightVar, {"Pythia", "Herwig"}, single);
        BOOST_TEST( models.size() == 2 );
        for (const auto& model: models)
            cout << model << '\n';
        cout << flush;

        BOOST_REQUIRE_NO_THROW( metainfo.GetVars(GenEvent::WeightVar) );
        BOOST_TEST( metainfo.GetVars(GenEvent::WeightVar).size() == models.size() );
        BOOST_REQUIRE_THROW( metainfo.GetVars(RecEvent::WeightVar), wrapexcept<invalid_argument> );

        BOOST_REQUIRE_THROW( metainfo.AddVars(GenEvent::WeightVar, {"MadGraph"}, single, 2), wrapexcept<invalid_argument> );

        metainfo.Set<int>("variations", "GenEvtWgts", "Vincia", 0);
        BOOST_REQUIRE_THROW( metainfo.GetVars(GenEvent::WeightVar, "Vincia"), wrapexcept<invalid_argument> );
    }

    BOOST_AUTO_TEST_CASE( JES_like )
    {
        auto JEC = metainfo.AddVars(RecJet::ScaleVar, {"FlavourQCD", "PtBalance"});
        BOOST_TEST( JEC.size() == 4 );
        for (const auto& jes: JEC)
            cout << jes << '\n';
        cout << flush;

        BOOST_TEST( metainfo.GetVars(RecJet::ScaleVar, "JER").size() == 0 );
        JEC.merge(metainfo.AddVars(RecJet::ScaleVar, {"JER"}));
        BOOST_TEST( JEC.size() == 6 );

        BOOST_TEST( metainfo.GetVars(RecJet::ScaleVar, "JER").size() == 2            );
        BOOST_TEST( metainfo.GetVars(RecJet::ScaleVar       ).size() == JEC.size()   );
        BOOST_TEST( metainfo.GetVars(                       ).size() == JEC.size()+1 );
        for (const auto& v: metainfo.GetVars())
            cout << v << '\n';
        cout << flush;

        BOOST_TEST_MESSAGE( "Testing an erroneous regex" );
        BOOST_REQUIRE_THROW( metainfo.GetVars(RecJet::ScaleVar, "\\w{"), wrapexcept<regex_error> );

        BOOST_TEST_MESSAGE( "Testing a wrong number of members" );
        BOOST_REQUIRE_THROW( metainfo.AddVars(RecJet::ScaleVar, {"JER"}, symmetric, 0), wrapexcept<invalid_argument> );
    }

    BOOST_AUTO_TEST_CASE( replica_like )
    {
        using enum Darwin::Physics::VarType;
        auto replicae = metainfo.AddVars(GenEvent::WeightVar, {"Replica"}, replicas, 10);
        BOOST_TEST( replicae.size() == 10 );
        for (const auto& replica: replicae)
            cout << replica << '\n';
        cout << flush;

        BOOST_TEST( metainfo.GetVars(GenEvent::WeightVar).size() == replicae.size() );
    }

    BOOST_AUTO_TEST_CASE( prefiring_like )
    {
        auto prefbins = metainfo.AddVars(RecEvent::WeightVar, {"Prefiring"}, symmetric | replicas, 20);
        BOOST_TEST( prefbins.size() == 40 );
        for (const auto& prefbin: prefbins)
            cout << prefbin << '\n';
        cout << flush;

        BOOST_TEST( metainfo.GetVars(RecEvent::WeightVar).size() == prefbins.size() );
    }

    BOOST_AUTO_TEST_CASE( bSF_like )
    {
        auto bSFstatbins = metainfo.AddVars(RecJet::WeightVar, {"bSFstat"}, symmetric | bits, 8);
        BOOST_TEST( bSFstatbins.size() == 16 );
        for (const auto& bSFstatbin: bSFstatbins)
            cout << bSFstatbin << '\n';
        cout << flush;

        BOOST_TEST( metainfo.GetVars(RecJet::WeightVar).size() == bSFstatbins.size() );
    }

BOOST_AUTO_TEST_SUITE_END()

#endif
