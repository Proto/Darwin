#ifndef DOXYGEN_SHOULD_SKIP_THIS

// Darwin
#include <Lepton.h>

// BOOST
#define BOOST_TEST_MODULE Lepton
#include <boost/test/included/unit_test.hpp>

// STD
#include <iostream>

using namespace Darwin::Physics;
using namespace std;

BOOST_AUTO_TEST_CASE( genlevel )
{
    GenMuon muon;
    BOOST_TEST( muon.scales.size() == 1 );
    BOOST_TEST( muon.weights.size() == 1 );
    BOOST_TEST( muon.Q == (int) 0 );
    cout << muon << endl;
}

BOOST_AUTO_TEST_CASE( reclevel )
{
    RecMuon muon;
    BOOST_TEST( muon.scales.size() == 1 );
    BOOST_TEST( muon.weights.size() == 1 );
    BOOST_TEST( muon.Q == 0 );
    BOOST_TEST( muon.selectors == 0 );
    cout << muon << endl;
}

#endif
