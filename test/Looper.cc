#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define DOXYGEN_SHOULD_SKIP_THIS
#define BOOST_TEST_MODULE Looper
#include <boost/test/included/unit_test.hpp>
#include <boost/exception/all.hpp>

#include <TTree.h>
#include "Looper.h"

using namespace std;
using namespace Darwin::Tools;

BOOST_AUTO_TEST_SUITE( looper )

    BOOST_AUTO_TEST_CASE( shared )
    {
        auto t = make_shared<TTree>("events", "events");

        // call empty tree
        BOOST_REQUIRE_THROW( Looper looper(t), boost::wrapexcept<invalid_argument> );

        // normal call (filling 100 events in two steps)
        for (Looper looper(100ll); looper(); ++looper) t->Fill();

        BOOST_REQUIRE( t->GetEntries() == 100 );
        BOOST_REQUIRE_NO_THROW( Looper looper(t) );
    }

    BOOST_AUTO_TEST_CASE( unique )
    {
        // Test constructor with a unique_ptr argument
        auto t = make_unique<TTree>("events", "events");
        BOOST_REQUIRE_THROW( Looper looper(t), boost::wrapexcept<invalid_argument> );
    }

BOOST_AUTO_TEST_SUITE_END()

#endif
