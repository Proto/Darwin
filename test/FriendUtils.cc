/// SPDX-License-Idendifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define DOXYGEN_SHOULD_SKIP_THIS
#define BOOST_TEST_MODULE testFriendUtils
#include <boost/test/included/unit_test.hpp>

#include <filesystem>

#include <TFile.h>
#include "FriendUtils.h"
#include "FileUtils.h"

using namespace std;
using namespace Darwin::Tools;

/**
 * Creates a file with a tree, and fills the \c index branch in the tree with
 * numbers in [first, last).
 */
void createFile(const char* name, int first, int last)
{
    TFile file(name, "RECREATE");
    TTree tree("", "");

    int index, value = 0;
    tree.Branch("index", &index);
    tree.Branch("value", &value); // This will be a constant 0

    for (index = first; index < last; ++index) tree.Fill();

    file.WriteObject(&tree, "tree");
    file.Close();
}

BOOST_AUTO_TEST_SUITE( friends )

    /**
     * Tests \c ChainSlice.
     */
    BOOST_AUTO_TEST_CASE( slice )
    {
        // Create two files with "index" ranging from 0 to 9 then 10 to 19.
        createFile("friends-ChainSlice-file0.root",  0, 10);
        createFile("friends-ChainSlice-file1.root", 10, 20);

        // Create a ChainSlice spanning part of the two input files.
        auto chain = make_unique<ChainSlice>("tree");
        chain->Add("friends-ChainSlice-file0.root");
        chain->Add("friends-ChainSlice-file1.root");

        // Initially the slice covers everything.
        BOOST_TEST( chain->GetBegin() == 0 );
        BOOST_TEST( chain->GetEnd() == -1 );
        BOOST_TEST( chain->GetEntries() == 20 );
        BOOST_TEST( chain->GetEntriesFast() == 20 );

        // Check that we can load value correctly.
        int index;
        chain->SetBranchAddress("index", &index);

        chain->GetEntry(5);  // From first file
        BOOST_TEST( chain->GetReadEntry() == 5 );
        BOOST_TEST( index == 5 );

        chain->GetEntry(15);  // From second file
        BOOST_TEST( chain->GetReadEntry() == 15 );
        BOOST_TEST( index == 15 );

        // Now we set Begin = 10  =>  only the second file is available
        chain->SetBegin(10);
        BOOST_TEST( chain->GetBegin() == 10 );
        BOOST_TEST( chain->GetEnd() == -1 );
        BOOST_TEST( chain->GetEntries() == 10 );
        BOOST_TEST( chain->GetEntriesFast() == 10 );

        chain->GetEntry(5);  // From second file
        BOOST_TEST( chain->GetReadEntry() == 5 );
        BOOST_TEST( index == 15 );

        // Remove the last 3 elements. What is left is [10,17[.
        chain->SetEnd(17);
        BOOST_TEST( chain->GetEntries() == 7 );
        BOOST_TEST( chain->GetEntriesFast() == 7 );
        BOOST_TEST( chain->GetReadEntry() == 5 ); // Changing the bounds does not do GetEntry()
    }

    BOOST_AUTO_TEST_CASE( pure_root_without_ranges )
    {
        createFile("friends-hybrid-step0-0.root",  0, 10);
        createFile("friends-hybrid-step0-1.root", 10, 20);

        // Create a slice spanning events 5 to 15.
        auto chain = make_unique<TChain>("tree");
        chain->Add("friends-hybrid-step0-0.root");
        chain->Add("friends-hybrid-step0-1.root");
        chain->GetEntry(0); // necessary to load a(ny) TTree

        {
            auto newTree = make_unique<TTree>("tree", "");

            // Add (empty) entries to the tree.
            for (int i = 0; i < chain->GetEntries(); ++i) {
                newTree->Fill();
            }

            // Save the tree to a file, otherwise GetEntry() doesn't load branches
            // from the friend.
            TFile f("friends-hybrid-step1.root", "RECREATE");
            f.WriteObject(newTree.get(), "tree");
        }

        // try to read with a TChain
        {
			auto tIn = new TChain("tree");
            tIn->Add("friends-hybrid-step1.root");
            tIn->AddFriend(chain.get(), "step0"); // chain with chain
            tIn->GetEntry(0);

            // Now iterate over the tree and check the contents of the branches
            // inherited from the friends.
            int index = -1, value = -1;
            BOOST_REQUIRE( tIn->SetBranchAddress("index", &index) == 0 );
            BOOST_REQUIRE( tIn->SetBranchAddress("value", &value) == 0 );

            for (int i = 0; i < tIn->GetEntries(); ++i) {
                BOOST_TEST( tIn->GetEntry(i) > 0 );
                BOOST_TEST( index == i );
                BOOST_TEST( value == 0 );
            }
        }
    }

    /**
     * Tests \c SlicedFriendElement.
     *
     * This test uses two input files and checks that we can load a subset of
     * them through another tree to which a \c SlicedFriendElement has been
     * added.
     */
    BOOST_AUTO_TEST_CASE( slice_element )
    {
        createFile("friends-SlicedFriendElement-step0-0.root",  0, 10);
        createFile("friends-SlicedFriendElement-step0-1.root", 10, 20);

        const int begin = 5, end = 15;
        {
            // Create a slice spanning events 5 to 15.
            auto inputSlice = make_unique<ChainSlice>("tree");
            inputSlice->Add("friends-SlicedFriendElement-step0-0.root");
            inputSlice->Add("friends-SlicedFriendElement-step0-1.root");
            inputSlice->SetBegin(begin);
            inputSlice->SetEnd(end);

            // Set up a new tree and give it the other two as friends.
            auto newTree = make_unique<TTree>("tree", "step1");
            SlicedFriendElement::AddTo(newTree.get(), inputSlice.get(), "step0");

            // Add (empty) entries to the tree.
            for (int i = 0; i < inputSlice->GetEntries(); ++i) {
                newTree->Fill();
            }

            // Save the tree to a file, otherwise GetEntry() doesn't load branches
            // from the friend.
            TFile f("friends-SlicedFriendElement-step1.root", "RECREATE");
            f.WriteObject(newTree.get(), "tree");
        }

        // try to read with a standard TChain
        {
			auto chainIn = make_unique<TChain>("tree");
            chainIn->Add("friends-SlicedFriendElement-step1.root");
            chainIn->GetEntry(0); // necessary for `TChain::GetTree()`
            auto tIn = chainIn->GetTree();
            auto friends = tIn->GetListOfFriends();
            auto fe = dynamic_cast<TFriendElement *>(friends->At(0));
            friends->Clear("nodelete");
            auto chainslice = fe->GetTree();
            chainIn->AddFriend(chainslice);
            chainIn->GetEntry(0); // necessary for `TChain::SetBranchAddress()`

            // Now iterate over the tree and check the contents of the branches
            // inherited from the friends.
            int index = -1, value = -1;
            BOOST_REQUIRE( chainIn->SetBranchAddress("index", &index) == 0 );
            BOOST_REQUIRE( chainIn->SetBranchAddress("value", &value) == 0 );

            // entry 0 in chainIn should be entry 5 in the first tree
            for (int i = 0; i < chainIn->GetEntries(); ++i) {
                BOOST_TEST( chainIn->GetEntry(i) > 0 );
                BOOST_TEST( index == i + begin );
                BOOST_TEST( value == 0 );
            }
        }

        // try to read with a custom ChainSlice
        {
			auto chainIn = make_unique<ChainSlice>("tree");
            chainIn->Add("friends-SlicedFriendElement-step1.root");
            chainIn->GetEntry(0); // all the gymnastic of the previous block is done inside the override of `GetEntry()`

            // Now iterate over the tree and check the contents of the branches
            // inherited from the friends.
            int index = -1, value = -1;
            BOOST_REQUIRE( chainIn->SetBranchAddress("index", &index) == 0 );
            BOOST_REQUIRE( chainIn->SetBranchAddress("value", &value) == 0 );

            for (int i = 0; i < chainIn->GetEntries(); ++i) {
                BOOST_TEST( chainIn->GetEntry(i) > 0 );
                BOOST_TEST( index == i + begin );
                BOOST_TEST( value == 0 );
            }
        }
    }

BOOST_AUTO_TEST_SUITE_END()

#endif
