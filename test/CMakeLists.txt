# SPDX-License-Identifier: GPLv3-or-later
#
# SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/test")

### Libraries

set(DARWIN_TOOLS Exceptions Flow FriendUtils Looper MetaInfo Options UserInfo)
set(DARWIN_PHYSICS Di Event Jet Lepton Photon GenericObject Variation Weight)

foreach(X ${DARWIN_TOOLS} ${DARWIN_PHYSICS})
    add_executable(${X} ${X}.cc)
    target_link_libraries(${X} Darwin)
    add_test(NAME ${X} COMMAND ${X})
endforeach()

set_tests_properties(Options PROPERTIES ENVIRONMENT "DARWIN_TABLES=dummy")

### Utilities
# Print the number of entries in a file containing 100 (the tree has no
# branches)
add_test(NAME printEntries_single
         COMMAND printEntries "${CMAKE_CURRENT_SOURCE_DIR}/data/printEntries_100.root")
set_tests_properties(printEntries_single PROPERTIES PASS_REGULAR_EXPRESSION "100")

# Same but with the same file twice (through a symlink)
add_test(NAME printEntries_multi
         COMMAND printEntries "${CMAKE_CURRENT_SOURCE_DIR}/data/printEntries_100.root ${CMAKE_CURRENT_SOURCE_DIR}/data/printEntries_100.root")
set_tests_properties(printEntries_multi PROPERTIES PASS_REGULAR_EXPRESSION "200")

# Files needed by main.cc
configure_file(example.info example.info COPYONLY)
add_executable(main main.cc)
target_link_libraries(main Darwin)
add_test(NAME Main COMMAND main)
set_tests_properties(Main PROPERTIES ENVIRONMENT "DARWIN_TABLES=.")

### Examples

foreach(X RANGE 1 4)
    add_executable(example0${X} example0${X}.cc)
    target_link_libraries(example0${X} Darwin)
    add_test(NAME example0${X} COMMAND example0${X})
endforeach()

### Scripts

# Files needed by examples and transcribe
foreach(X examples scenario transcribe)
    configure_file(${X}.sh ${X} COPYONLY)
endforeach()
add_test(NAME Examples COMMAND examples)
set_tests_properties(Examples PROPERTIES ENVIRONMENT "DARWIN_TABLES=.")

#add_test(NAME Job COMMAND job)  # FIXME requires a condor cluster
add_test(NAME Transcribe COMMAND transcribe)
set_tests_properties(Transcribe PROPERTIES ENVIRONMENT "DARWIN_TABLES=.")
