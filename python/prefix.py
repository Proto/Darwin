#!/usr/bin/env python3

import math
import multiprocessing
import os
import re
import shlex
import socket
import stat
import sys
import textwrap
from argparse import ArgumentParser
from glob import glob
from pathlib import Path
from shutil import copy2, which
from subprocess import check_output
from typing import List

__all__ = [
    "PrefixCommand",
    "preparse",
    "tweak_helper_multi",
    "diagnostic",
    "git_hash",
    "copy_condor_config",
    "find_institute",
]

NPROC = multiprocessing.cpu_count()


def preparse(
    argv: List[str],
    tutorial: str,
    multi_opt: bool = False,
    dag_opt: bool = False,
    condor: bool = False,
):
    """Parser for the prefix command itself, relying on ArgumentParser."""

    parser = ArgumentParser(add_help=False)

    parser.add_argument("exec", nargs="?")
    if multi_opt:
        parser.add_argument("-b", "--background", action="store_true")
        parser.add_argument("-d", "--dry-run", action="store_true")
    if dag_opt:
        parser.add_argument("-D", "--dag", type=Path, nargs='?', default="dag")
    if condor:
        parser.add_argument("-n", "--memory-needs", type=int, default=1024)

    # helper
    parser.add_argument("-h", "--help", action="store_true")
    parser.add_argument("-t", "--tutorial", action="store_true")
    parser.add_argument("-g", "--git", action="store_true")
    if not dag_opt:
        parser.add_argument("-e", "--example", action="store_true")

    # common
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("-m", "--mute", action="store_true")
    parser.add_argument("-f", "--fill", action="store_true")
    parser.add_argument("-F", "--Friend", action="store_true")
    parser.add_argument("-s", "--syst", action="store_true")
    parser.add_argument("-c", "--config", type=Path)
    parser.add_argument("-j", "--nSplit", type=int, default=NPROC)

    # custom will be in args
    cmds, args = parser.parse_known_intermixed_args(argv[1:])

    if any(x in args for x in ["-k", "--nNow"]):
        raise ValueError("Giving `-k` or `--nNow` is meaningless in this context.")
    unknown_explicit_args = [arg for arg in args if arg[0] == "-"]
    if len(unknown_explicit_args) > 0:
        raise ValueError(
            f"No unknown explicit option is allowed: {unknown_explicit_args}"
        )

    # restore explicit options
    if cmds.verbose:
        args += ["-v"]
    if cmds.mute:
        args += ["-m"]
    if cmds.fill:
        args += ["-f"]
    if cmds.Friend:
        args += ["-F"]
    if cmds.syst:
        args += ["-s"]
    if cmds.config:
        args += ["-c", str(cmds.config.resolve())]

    if not cmds.exec:
        if cmds.help or len(argv) == 1:
            prefix = os.path.basename(sys.argv[0])
            print(
                f"\33[1m{prefix} exec args [...]\33[0m",
                "where\texec = a command using `Darwin::Tools::Options` with `Darwin::Tools::split`",
                "\targs = the arguments of the same command, one being called exactly `output`",
                sep="\n",
            )
        if cmds.tutorial:
            # boost::program_options::options_description::m_default_line_length = 80
            lines = textwrap.TextWrapper(width=80).wrap(text=tutorial)
            print(*lines, sep="\n")
        parser.exit()

    return cmds, args


def git_hash(exec: str) -> None:
    """Returns the SHA of the executable at compile time."""
    print(check_output(exec + " -g", shell=True).decode(), end="")


def find_institute():
    """Identifies the host from the name of the machine"""

    clusters = {
        r"naf-cms[1-2][0-9].desy.de": "DESY",
        r".phy.pku.edu.cn": "PKU",
        r"lxplus[0-9]*.cern.ch": "CERN",
    }

    hostname = socket.gethostname()
    for key, value in clusters.items():
        if re.search(key, hostname):
            return value

    raise RuntimeError("The host could not be identified")


class PrefixCommand:
    """General utilities for prefix commands (try, parallel, submit).
    The explicit options (`cmds`) are unchanged; among the implicit options,
    the inputs are checked (if any), and the output is adapted to each thread."""

    name: str = ""
    cmds: List[str] = []
    inputs: List[str] = []
    absinputs: List[str] = []
    output: str = ""
    absoutput: Path = ""
    args: List[str] = []
    helper: str = ""
    extension: str = ""

    def __init__(self, prefix: str, cmds: List[str]) -> None:
        """Constructor from a separated list of arguments."""

        self.name = os.path.basename(prefix)
        self.cmds = cmds

        self.helper = self._help_text()
        if self.helper[0].count("output") != 1:
            raise ValueError(
                f"`{self.cmds.exec}`"
                + " does not match the requirements to be run with this prefix command: "
                + "there must be exactly one `output` argument."
            )

        self.splittable = ["nSplit" in row for row in self.helper[1:]].count(True)
        self.nSplit = cmds.nSplit if self.splittable else 1

        self.extension = (
            check_output([self.cmds.exec] + ["-o"])
            .decode()
            .strip()
            .split("\n")[0]
            .split(" ")[0]
        )

    def _help_text(self) -> str:
        """Returns the command's normal help text."""
        # if executable is given, then the helper is adapted to match with the prefix command
        text = (
            check_output(self.cmds.exec + " -h", shell=True)
            .decode()
            .strip()
            .split("\n")
        )
        return text

    def _synopsis(self) -> List[str]:
        """Extract synopsis from helper (after undoing the bold characters)."""
        synopsis = self.helper[0].replace("\x1b[1m", "").replace("\x1b[0m", "")
        return synopsis.split()[1:]

    def parse(self, args: List[str]) -> None:
        """Put input(s), output, and other arguments in dedicated members."""

        synopsis = self._synopsis()
        i = synopsis.index("output")
        if i >= len(args):
            raise ValueError("Unable to identify `output` in given arguments")

        self.inputs = args[:i]
        self.output = args[i]
        if len(args) > i:
            self.args = args[i + 1 :]

    def get_entries(self, input) -> int:
        """Obtain the number of entries from input n-tuple."""

        output = check_output(["printEntries"] + [input.strip('\\"')])
        nevents = int(output.decode().splitlines()[0].replace("\n", ""))
        if nevents == 0:
            raise ValueError("Empty input tree.")
        return nevents

    def _prepare_single_output(self) -> None:
        """Prepare the working area for a single-run command."""

        if self.absoutput.exists():
            if not os.access(self.absoutput, os.W_OK):
                raise ValueError(self.absoutput + " is not writable")
            if self.absoutput.is_dir():
                self.absoutput /= "0" + self.extension
        else:
            if len(self.absoutput.suffix) == 0:  # then assume it's a directory
                self.absoutput.mkdir(parents=True)
                self.absoutput /= "0" + self.extension

        if self.absoutput.is_dir():
            self._save_environment()

    def _prepare_multi_output(self) -> None:
        """Prepare the members for parallel running (local or on farm)."""

        if self.absoutput.suffix != "":
            raise ValueError(
                f"A directory with an extension is misleading: {self.output}"
            )

        if self.absoutput.exists():
            if self.absoutput.is_file():
                raise ValueError(self.output + " is not a directory")

            new_rootfiles = []
            for k in range(self.nSplit):
                rootfile = self.absoutput / f"{k}{self.extension}"
                if rootfile.exists() and not os.access(rootfile, os.W_OK):
                    raise ValueError(
                        "The root files in " + self.output + " are not writable"
                    )
                new_rootfiles += [str(rootfile)]

            existing_rootfiles = glob(str(self.absoutput / f"*{self.extension}"))
            if len(set(existing_rootfiles).difference(new_rootfiles)) > 0:
                raise ValueError(
                    "Residual files from a former run will not be overwritten."
                )
        else:
            self.absoutput.mkdir(parents=True)

        self._save_environment()

    def _prepare_input(self) -> None:
        """Resolve the paths to the input(s)."""

        for x in self.inputs:
            self.absinputs += glob(x)
        if len(self.inputs) > 0 and len(self.absinputs) == 0:
            raise ValueError("Inputs could not be found: " + " ".join(self.inputs))

        self.absinputs = [str(Path(x).resolve()) for x in self.absinputs]

    def _save_environment(self) -> None:
        """Save environment in a file."""

        env = self.absoutput / ".env"
        with open(env, "w") as f:
            for key, value in os.environ.items():
                if key in ["PWD", "OLDPWD", "TMPDIR"]:
                    continue
                if "BASH" in key:
                    continue
                value = shlex.quote(value)
                print(f"export {key}={value}", file=f)
        os.chmod(env, stat.S_IRWXU)

    def prepare_io(self, multi: bool = True) -> None:
        """Prepare input and output files and directories."""

        self._prepare_input()
        self.absoutput = Path(self.output).resolve()

        if multi:
            self.inputs = [shlex.quote(x) for x in self.inputs]
            self._prepare_multi_output()
        else:
            self._prepare_single_output()

    def prepare_fire_and_forget(self) -> None:
        """Copies the executable, libraries, and dictionaries to the output directory."""

        # copy executables and libraries to working directory
        exec = Path(which(self.cmds.exec)).resolve()
        copy_exec = self.absoutput / exec.name
        if exec != copy_exec:
            copy2(exec, self.output)
            exec = copy_exec
        self.cmds.exec = str(exec)

        for ext in ["so", "pcm"]:
            for file in glob(os.environ["DARWIN_FIRE_AND_FORGET"] + f"/*{ext}"):
                copy2(file, self.output)

    def clean_up_last_run(self) -> None:
        """Removes the executable, libraries, hidden files, and dictionaries from the output directory."""

        exec = Path(which(self.cmds.exec)).name

        for file in [exec, ".env", ".condor"]:
            file = self.absoutput / Path(file)
            if file.is_file():
                os.remove(str(file))

        for ext in ["so", "pcm"]:
            for file in glob(str(self.absoutput) + f"/*{ext}"):
                print("Removing " + file)
                os.remove(file)


def tweak_helper_multi(
    prefix: PrefixCommand,
    multi_opt: bool = True,
    dag_opt: bool = False,
    condor: bool = False,
) -> None:
    """Tweak helper to use a directory as output rather than a file
    and to reflect the number of cores of the machine."""

    print(f"\33[1m{prefix.name} \33[0m", end="")
    for row in prefix.helper:
        if "output" in row:
            row = row.replace("ROOT file", "directory")
        if "nNow" in row:
            continue
        if "nSplit" in row:
            row = row.replace(
                "(=1)" if NPROC < 10 else "(=1) ", "(=" + str(NPROC) + ")"
            )
        print(row)
        if "Helper" in row:
            if multi_opt:
                print("  -b [ --background ]       Do not wait for job to finish")
                print("  -d [ --dry-run ]          Run until the actual executation")
            if dag_opt:
                print("  -D [ --dag ]              Indicate DAG file")
            if condor:
                print("  -n [ --memory-needs] arg  Memory needs for HTCondor job")
    print("")


def diagnostic(exception: Exception) -> None:
    """Common diagnostic message for all prefix commands in case of exception."""
    prefix = os.path.basename(sys.argv[0])
    versions = (
        check_output("printDarwinSoftwareVersion", shell=True)
        .decode()
        .strip()
        .split("\n")
    )
    versions += [f"Python {sys.version_info.major}.{sys.version_info.minor}"]
    print(
        f"\x1B[31m{prefix}: \x1B[1m{exception}\x1B[22m",
        ", ".join(versions),
        "Consider opening a GitLab issue if you don't understand the cause of the failure.\x1B[0m",
        sep="\n",
    )


def print_slice(formatting, line, nNow, nSplit, end):
    """
    Prefix the output stream with the slice. The formatting of the
    printout is preserved but the slice is printed in default formatting.
    """

    RESET = "\x1B[0m"

    # find the special characters to encode the colour and the highlighting
    special_chars = re.findall(r"\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])", line)

    # if any was found, check if it should be reset to default
    if special_chars and special_chars[-1] == RESET:
        formatting = RESET
    else:
        formatting += "".join(special_chars)

    print(f"{nNow}/{nSplit}\t{formatting}{line}{RESET}", end=end)

    return formatting


def copy_condor_config(output):
    """Copy the HTC config to the running directory."""

    copy2(Path(__file__).parent / "condor.jdl", str(output) + "/.condor")
